# [v2.4.7](https://gitlab.com/opencpi/osp/ocpi.osp.analog/-/compare/v2.4.6...v2.4.7) (2024-01-09)

Changes/additions since [OpenCPI Release v2.4.6](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.6)

### Bug Fixes
- **devops**: prevent commits from automatically launching a CI/CD pipeline. (!10)(ec4368a9)

# [v2.4.6](https://gitlab.com/opencpi/osp/ocpi.osp.analog/-/compare/v2.4.5...v2.4.6) (2023-03-29)

No Changes/additions since [OpenCPI Release v2.4.5](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.5)

# [v2.4.5](https://gitlab.com/opencpi/osp/ocpi.osp.analog/-/compare/v2.4.4...v2.4.5) (2023-03-16)

No Changes/additions since [OpenCPI Release v2.4.4](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.4)

# [v2.4.4](https://gitlab.com/opencpi/osp/ocpi.osp.analog/-/compare/v2.4.3...v2.4.4) (2023-01-22)

No Changes/additions since [OpenCPI Release v2.4.3](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.3)

# [v2.4.3](https://gitlab.com/opencpi/osp/ocpi.osp.analog/-/compare/v2.4.2...v2.4.3) (2022-10-11)

No Changes/additions since [OpenCPI Release v2.4.2](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.2)

# [v2.4.2](https://gitlab.com/opencpi/osp/ocpi.osp.analog/-/compare/v2.4.1...v2.4.2) (2022-05-26)

No Changes/additions since [OpenCPI Release v2.4.1](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.1)

# [v2.4.1](https://gitlab.com/opencpi/osp/ocpi.osp.analog/-/compare/v2.4.0...v2.4.1) (2022-03-16)

No Changes/additions since [OpenCPI Release v2.4.0](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.0)

# [v2.4.0](https://gitlab.com/opencpi/osp/ocpi.osp.analog/-/compare/v2.3.4...v2.4.0) (2022-01-25)

No Changes/additions since [OpenCPI Release v2.3.4](https://gitlab.com/opencpi/opencpi/-/releases/v2.3.4)

# [v2.3.4](https://gitlab.com/opencpi/osp/ocpi.osp.analog/-/compare/v2.3.3...v2.3.4) (2021-12-17)

No Changes/additions since [OpenCPI Release v2.3.3](https://gitlab.com/opencpi/opencpi/-/releases/v2.3.3)

# [v2.3.3](https://gitlab.com/opencpi/osp/ocpi.osp.analog/-/compare/v2.3.2...v2.3.3) (2021-11-30)

No Changes/additions since [OpenCPI Release v2.3.2](https://gitlab.com/opencpi/opencpi/-/releases/v2.3.2)

# [v2.3.2](https://gitlab.com/opencpi/osp/ocpi.osp.analog/-/compare/v2.3.1...v2.3.2) (2021-11-08)

No Changes/additions since [OpenCPI Release v2.3.1](https://gitlab.com/opencpi/opencpi/-/releases/v2.3.1)

# [v2.3.1](https://gitlab.com/opencpi/osp/ocpi.osp.analog/-/compare/v2.3.0...v2.3.1) (2021-10-13)

No Changes/additions since [OpenCPI Release v2.3.0](https://gitlab.com/opencpi/opencpi/-/releases/v2.3.0)

# [v2.3.0](https://gitlab.com/opencpi/osp/ocpi.osp.analog/-/compare/v2.3.0-rc.1...v2.3.0) (2021-09-07)

Changes/additions since [OpenCPI Release v2.3.0-rc.1](https://gitlab.com/opencpi/opencpi/-/releases/v2.3.0-rc.1)

### Enhancements
- **devops**: enable project's CI pipeline. (!3)(43fbcecd)

### Bug Fixes
- **doc**: add file "index.rst" to top level of `analog` OSP. (!6)(761b8a77)
- **osp**: remove unused i2c subdevice worker from e31x hdl platform assemblies. (!5)(00ed2089)

# [v2.3.0-rc.1](https://gitlab.com/opencpi/osp/ocpi.osp.analog/-/compare/8964f85c...v2.3.0-rc.1) (2021-08-25)

Initial release of ADRV9361-Z7035 attached to ADRV1CRR-BOB for OpenCPI
