.. adrv9361_gsg Analog Devices ADRV9361-Z7035 Getting Started Guide documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _adrv9361_gsg:

.. |trade| unicode:: U+2122
   :ltrim:

.. |reg| unicode:: U+00AE
   :ltrim:

      
OpenCPI Analog Devices ADRV9361-Z7035 Getting Started Guide
===========================================================

This document provides installation information that is specific
to the Analog Devices\ |trade| ADRV9361-Z7035 System on a Module (SOM).  
Use this document when performing the tasks described
in the chapter "Enabling OpenCPI Development for Embedded Systems"
in the 
`OpenCPI Installation Guide <https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Installation_Guide.pdf>`_.
This document supplies details about enabling OpenCPI development on the ADRV9361-Z7035 SOM 
that can be applied to the procedures described in the referenced *OpenCPI Installation Guide* chapter.

The following documents can also be used as reference to the tasks described in this document:

* `OpenCPI User Guide <https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_User_Guide.pdf>`_
  
* `OpenCPI Glossary <https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Glossary.pdf>`_

Note that the *OpenCPI Glossary* is also contained in both the *OpenCPI Installation Guide* and the
*OpenCPI User Guide*.

This document assumes a basic understanding of the Linux command line (or "shell") environment.

Document Revision History
-------------------------

.. csv-table:: OpenCPI Analog Devices ADRV9361-Z7035 Getting Started Guide: Revision History
   :header: "OpenCPI Version", "Description of Change", "Date"
   :widths: 10,30,10
   :class: tight-table

   "v2.3", "Initial Release", "6/2021"


Overview
--------

The ADRV9361-Z7035 is a System on a Module from Analog Devices.
See the Analog Devices `<https://wiki.analog.com/resources/eval/user-guides/adrv9361-z7035>`_ 
User Guide for more information about the module. 

The OpenCPI HDL (FPGA) platform for the ADRV9361-Z7035 is called 
the "``adrv9361``". 

OpenCPI has been tested on the ADRV9361-Z7035 mated with ADRV1CRR-BOB breakout board.  

.. figure:: img/top.jpg
   :alt: 
   :align: center
   :width: 400

   ADRV9361-Z7035 Connected to ADRV1CRR-BOB Carrier Board

:numref:`adrv9361-hw` lists the items required in setting up the ADRV9361-Z7035 for OpenCPI installation and deployment.

.. _adrv9361-hw:

.. csv-table:: ADRV9361-Z7035 Items and their Use in OpenCPI Installation and Deployment
   :header: "Item", "Used for..."
   :widths: 40,60
   :class: tight-table

   "ADRV9361-Z7035 SOM", "OpenCPI target embedded system"
   "ADRV1CRR-BOB", "Breakout Board Carrier board for target embedded system" 
   "Power supply", "+5V Power source for ADRV9361-Z7035 SOM"
   "1 Gigabit Ethernet cable", ":ref:`net-mode`"
   "Micro-USB-to-USB cable", ":ref:`serial-console`"
   "Standard SD card (8GB or larger)", ":ref:`sd-card-setup` (Contents not used for OpenCPI installation/deployment)"
   "U.FL-to-SMA adapters (2)", "Not used for OpenCPI installation/deployment"
   "Getting started guide", "General information about ADRV9361-Z7035."

Enabling OpenCPI Development for the ADRV9361-Z7035 SOM
-------------------------------------------------------

This section gives information about the ADRV9361-Z7035 SOM to use when
following the instructions in the chapter
“Enabling OpenCPI Development for Embedded Systems” of the
`OpenCPI Installation Guide <https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Installation_Guide.pdf>`_.

.. note::

   The instructions in the next sections assume that the basic OpenCPI installation for the development host has already been done.
   If it hasn't, follow the instructions in the chapter "Installing OpenCPI on Development Hosts" of the
   `OpenCPI Installation Guide <https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Installation_Guide.pdf>`_
   *before* proceeding to follow the instructions in these sections.


Installation Steps for the ADRV9361 Platform
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This section contains information to be applied to the tasks described
in the section "Installation Steps for Platforms" of the `OpenCPI Installation Guide <https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Installation_Guide.pdf>`_.


Supported OpenCPI Platforms and Vendor Tools
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:numref:`sfw-reqs` lists the OpenCPI software (RCC)
and hardware (HDL (FPGA) platforms used for the ADRV9361-Z7035 SOM
and the third-party/vendor tools on which they depend.

.. I used ascii art for this table to be able to control line breaks in column text.
   
.. Need to find out how to turn off "no line wrap" in HTML renderer so that column text will wrap in csv-table and list-table.

.. _sfw-reqs:

.. table:: Supported OpenCPI Platforms for the ADRV9361-Z7035 and their Dependencies
	   
   +------------------------+-------------------+---------------------+---------------------------------------+
   | OpenCPI                + Description       + OpenCPI             + Required Third-Party/                 |
   |                        +                   +                     +                                       |
   | Platform Name          +                   + Project/Repo        + Vendor Tools                          |
   +========================+===================+=====================+=======================================+
   | ``xilinx19_2_aarch32`` + Xilinx Linux from + ``ocpi.core``       + Xilinx Vitis\ |trade| SDK 2019.2      |
   |                        +                   +                     +                                       |
   |                        + 2019Q2 (2019.2)   + built-in            + Xilinx Binary Zynq Release 2019.2     |
   |                        +                   +                     +                                       |
   |                        + for Zynq-7000     +                     + Xilinx Linux ``git`` clone            |
   |                        +                   +                     +                                       |
   |                        +                   +                     + Xilinx Linux tag: ``xilinx-v2019.2``  |
   +------------------------+-------------------+---------------------+---------------------------------------+
   | ``adrv9361``           + Analog ADRV9361   + ``ocpi.osp.analog`` + Xilinx Vivado\ |reg| Licensed         |
   |                        +                   +                     +                                       |
   |                        + Zynq FPGA/PL      +                     +                                       |
   +------------------------+-------------------+---------------------+---------------------------------------+


Notes about the OpenCPI platforms listed in the table:

* ``xilinx19_2_aarch32`` is the primary RCC platform for the ADRV9361-Z7035 SOM.

* The ``xilinx19_2_aarch32`` RCC platform is configured for DHCP.

* The OSP for the ``adrv9361`` HDL platform is named **analog** to plan for possible future support of other analog devices platforms.


Installing the Vendor Tools for the OpenCPI Platforms
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Follow the instructions in the section "Installing Xilinx Tools"
in the chapter “Installing Third-party/Vendor Tools” in the
`OpenCPI Installation Guide <https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Installation_Guide.pdf>`_
to install the Xilinx tools listed as platform dependencies in :numref:`sfw-reqs`.

.. note::
   
   Perform this step first, *before* installing/building the OpenCPI platforms for the ADRV9361-Z7035.
 
Installing and Building the OpenCPI Platforms for the ADRV9361-Z7035
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
After installing the required vendor tools, follow the instructions
in the section “Installation Steps for Platforms” in the
`OpenCPI Installation Guide <https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Installation_Guide.pdf>`_
to install and build the ``xilinx19_2aarch32`` and ``adrv9361`` platforms for the ADRV9361-Z7035.

Both these platforms must be installed and built with ``ocpiadmin install platform``
before proceeding to set up the SD card for the ADRV9361-Z7035. For example:

.. code-block:: bash

   ocpiadmin install platform xilinx19_2_aarch32
   
   ocpiadmin install platform adrv9361

See the `ocpiadmin(1) man page <https://opencpi.gitlab.io/releases/latest/man/ocpiadmin.1.html>`_ for command usage details.

.. note::
   
   The ``xilinx19_2_aarch32`` platform is also used by other systems, such as the
   Avnet (Digilent) ZedBoard (in OpenCPI, the ``zed`` and ``zed_ise`` HDL platforms).
   If the ``xilinx19_2_aarch32`` platform has already been installed and built
   for another supported system, it can apply to the ADV9361-Z7035 and does not need to be installed and built.

Building the platforms results in a single executable FPGA
test application named ``bias`` that is ready to run. It can be used to verify
the installation and any assemblies of the adrv9361 OSP itself. The ``bias``
test application is based on the ``testbias`` HDL assembly.
Both ``bias`` and ``testbias`` reside in the ``ocpi.assets`` built-in project.

Installation Steps for the ADRV9361-Z7035 After Its Platforms Have Been Installed
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This section contains information to be applied to the tasks described
in the section "Installation Steps for
Embedded Systems After Their Platforms Have Been Installed" in the
`OpenCPI Installation Guide <https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Installation_Guide.pdf>`_.

.. _net-mode:

Setting Up the ADRV9361-Z7035 for OpenCPI Network-Mode Operation (Optional)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

OpenCPI allows three modes of operation on embedded systems: *network mode*, *standalone mode*, and *server mode*
(see the beginning paragraphs in the section "Installation Steps for Systems after their Platforms are Installed"
of the `OpenCPI Installation Guide <https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Installation_Guide.pdf>`_ for details).

Using network mode for OpenCPI on the ADRV9361-Z7035 requires establishing an Ethernet connection from
the ADRV9361-Z7035 SOM to a network that supports DHCP.

There is one Ethernet port located on the ADRV1CRR-BOB carrier board.
Use the 1-Gigabit Ethernet cable to connect this port to the DHCP-supported network.

.. _sd-card-setup:

Setting up the SD Card for the ADRV9361-Z7035
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The ADRV9361-Z7035 boots from a SD card.  Enabling OpenCPI on
the ADRV9361-Z7035 requires creating a bootable SD card that replaces
the factory SD card supplied with the system on module.

Follow the instructions in the subsections "Preparing the SD Card Contents" and "Writing the SD Card"
in the section "Installation Steps for Systems After Their Platforms are Installed" of the
`OpenCPI Installation Guide <https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Installation_Guide.pdf>`_
to create a bootable SD card that enables the ADRV9361 for OpenCPI.  

.. note::

   The recommended method described in the *OpenCPI Installation Guide* is to make a raw copy of the manufacturer-supplied
   card to a new card, preserving the manufacturer's formatting and content, and then remove the original files
   from the raw copy and copy the files from OpenCPI.

   For the ADRV9361, be sure to remove all the original contents from the raw copy you create from the factory SD card.
   OpenCPI provides all of the necessary files for installation and deployment on the ADRV9361, and the presence
   of any factory-supplied contents can cause problems with this process.

Should you need to format an SD card for the ADRV9361, it should be a single FAT32 partition.

The bootable SD card slot is located on the ADRV9361-Z7035 SOM. To eject an SD card, gently slide the holder to 
insert the SD card then slide it back to lock it in.

.. _front-panel-diagram:

.. figure:: img/sd_card.jpg
   :alt: 
   :align: center
   :width: 400

   ADRV9361-Z7035 SOM: Top SD Card Holder

.. note::

   The ADRV9361-Z7035 SOM provides 10 U.FL (50 Ohm) connectors.
   These connectors are not relevant to enabling OpenCPI on the ADRV9361-Z7035.  See the Analog Devices
   `ADRV9361-Z7035 User Guide <https://wiki.analog.com/resources/eval/user-guides/adrv9361-z7035>`_ for details
   about their function.
 

.. _serial-console:

Establishing a Serial Console Connection for the ADRV9361-Z7035 SOM
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Follow the instructions in the section "Establishing a Serial Console Connection"
in the `OpenCPI Installation Guide <https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Installation_Guide.pdf>`_
to set up a serial console connection between the ADRV9361-Z7035 and the development host. 

On the ADRV1CRR-BOB, the micro-USB serial port
is located on the carrier board's IO corner of the PCB (see :numref:`carrier-board-diagram`) and is labeled **USB-UART**.
Connect the micro-USB to USB-A cable supplied in the product package
from this port to the development host.

The ADRV9361-Z7035's console serial port operates at 115200 baud.


.. _carrier-board-diagram:

.. figure:: img/uart_bob.jpg
   :alt: 
   :align: center
   :width: 400

   ADRV9361-Z7035 SOM: USB to Serial Console Port

Configuring the Runtime Environment on the adrv9361 Platform
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Follow the instructions in the section
"Configuring the Runtime Environment on the Embedded System"
in the `OpenCPI Installation Guide <https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Installation_Guide.pdf>`_.

The ADRV9361-Z7035 is initially set with ``root`` for user name and password.


Running the Reference Application
---------------------------------

Once you have successfully set up OpenCPI and the ADRV9361-Z7035 SOM, you can run
the FSK Digital Radio Controller reference
application, which is located in
``projects/assets/applications/fsk_dig_radio_ctrlr``.

Documentation for this reference application is currently unavailable.
