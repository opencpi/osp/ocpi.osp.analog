# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along
# with this program. If not, see <http://www.gnu.org/licenses/>.

# this is the adrv9361 XDC file that should be generated when instantiating the:
# platform_ad9361_dac_sub worker
# on the adrv1crr_bob card

############################################################################
# Clock constraints                                                        #
############################################################################
# 10 ns period = 100000 KHz
create_clock -name clk_fpga_1 -period 10.000 [get_pins -hier * -filter {NAME =~ */ps/PS7_i/FCLKCLK[1]}]

# ----------------------------------------------------------------------------
# Clock constraints - platform_ad9361_data_sub.hdl
# ----------------------------------------------------------------------------

# AD9361 DATA_CLK_P

# create_clock command defaults to 50% duty cycle when -waveform is not specified

# from AD9361 datasheet
set AD9361_LVDS_t_CP_ns 4.069
create_clock -period $AD9361_LVDS_t_CP_ns -name platform_ad9361_data_sub_DATA_CLK_P [get_ports {platform_ad9361_data_sub_DATA_CLK_P}]

# AD9361 FB_CLK_P (forwarded version of DATA_CLK_P)
create_generated_clock -name platform_ad9361_data_sub_FB_CLK_P -source [get_pins {ftop/pfconfig_i/platform_ad9361_data_sub_i/worker/mode7.dac_clock_forward/C}] -divide_by 1 -invert [get_ports {platform_ad9361_data_sub_FB_CLK_P}]


#############################
### from ccbob_constr.xdc ###
#############################

## constraints (ccbrk.c + ccbrk_lb.a)
## platform_ad9361 clkout forward

set_property  -dict {PACKAGE_PIN  A7      IOSTANDARD  LVCMOS18} [get_ports  IO_L18_34_JX4_N]        ; ## (lb: gpio_bd[15])  U1,A7,IO_L18_34_JX4_N,JX4,70,IO_L18_34_JX4_N,P7,32

## push-buttons- led- dip-switches- loopbacks- (ps7 gpio)

set_property  -dict {PACKAGE_PIN  J3      IOSTANDARD  LVCMOS18} [get_ports  IO_L12_MRCC_33_JX1_N]   ; ## (lb: gpio_bd[4])   U1,J3,IO_L12_MRCC_33_JX1_N,JX1,83,PB_GPIO_0,P4,31
set_property  -dict {PACKAGE_PIN  D8      IOSTANDARD  LVCMOS18} [get_ports  IO_L08_34_JX4_N]        ; ## (lb: gpio_bd[5])   U1,D8,IO_L08_34_JX4_N,JX4,38,PB_GPIO_1,P6,19
set_property  -dict {PACKAGE_PIN  F9      IOSTANDARD  LVCMOS18} [get_ports  IO_L09_34_JX4_P]        ; ## (lb: gpio_bd[6])   U1,F9,IO_L09_34_JX4_P,JX4,41,PB_GPIO_2,P6,26
set_property  -dict {PACKAGE_PIN  E8      IOSTANDARD  LVCMOS18} [get_ports  IO_L09_34_JX4_N]        ; ## (lb: gpio_bd[12])  U1,E8,IO_L09_34_JX4_N,JX4,43,PB_GPIO_3,P6,28
set_property  -dict {PACKAGE_PIN  A8      IOSTANDARD  LVCMOS18} [get_ports  IO_L17_34_JX4_N]        ; ## (lb: gpio_bd[0])   U1,A8,IO_L17_34_JX4_N,JX4,69,LED_GPIO_0,P7,16
set_property  -dict {PACKAGE_PIN  W14     IOSTANDARD  LVCMOS25} [get_ports  IO_00_12_JX4]           ; ## (lb: gpio_bd[1])   U1,W17,IO_25_12_JX4,JX4,16,LED_GPIO_2,P13,3
set_property  -dict {PACKAGE_PIN  W17     IOSTANDARD  LVCMOS25} [get_ports  IO_25_12_JX4]           ; ## (lb: gpio_bd[2])   U1,W14,IO_00_12_JX4,JX4,14,LED_GPIO_1,P13,4
set_property  -dict {PACKAGE_PIN  Y16     IOSTANDARD  LVCMOS25} [get_ports  IO_L23_12_JX2_P]        ; ## (lb: i2c_scl)      U1,Y16,IO_L23_12_JX2_P,JX2,97,LED_GPIO_3,P2,4 (U1,AF24,SCL,JX2,17,I2C_SCL,P2,14)
set_property  -dict {PACKAGE_PIN  Y15     IOSTANDARD  LVCMOS25} [get_ports  IO_L23_12_JX2_N]        ; ## (lb: none)         U1,Y15,IO_L23_12_JX2_N,JX2,99,DIP_GPIO_0
set_property  -dict {PACKAGE_PIN  W16     IOSTANDARD  LVCMOS25} [get_ports  IO_L24_12_JX4_P]        ; ## (lb: none)         U1,W16,IO_L24_12_JX4_P,JX4,13,DIP_GPIO_1
set_property  -dict {PACKAGE_PIN  W15     IOSTANDARD  LVCMOS25} [get_ports  IO_L24_12_JX4_N]        ; ## (lb: none)         U1,W15,IO_L24_12_JX4_N,JX4,15,DIP_GPIO_2
set_property  -dict {PACKAGE_PIN  V19     IOSTANDARD  LVCMOS25} [get_ports  IO_00_13_JX2]           ; ## (lb: none)         U1,V19,IO_00_13_JX2,JX2,13,DIP_GPIO_3

## orphans- io- (ps7 gpio)

set_property  -dict {PACKAGE_PIN  V18     IOSTANDARD  LVCMOS25} [get_ports  IO_25_13_JX2]           ; ## (lb: gpio_bd[3])   U1,V18,IO_25_13_JX2,JX2,14,IO_25_13_JX2,P2,3
set_property  -dict {PACKAGE_PIN  AB24    IOSTANDARD  LVCMOS25} [get_ports  IO_L06_13_JX2_N]        ; ## (lb: i2c_sda)      U1,AB24,IO_L06_13_JX2_N,JX2,20,IO_L06_13_JX2_N,P2,15 (U1,AF25,SDA,JX2,19,I2C_SDA,P2,16)
set_property  -dict {PACKAGE_PIN  AA24    IOSTANDARD  LVCMOS25} [get_ports  IO_L06_13_JX2_P]        ; ## (lb: none)         U1,AA24,IO_L06_13_JX2_P,JX2,18,IO_L06_13_JX2_P
set_property  -dict {PACKAGE_PIN  N8      IOSTANDARD  LVCMOS18} [get_ports  IO_25_33_JX1]           ; ## (lb: clkout_out)   U1,N8,IO_25_33_JX1,JX1,10,IO_25_33_JX1,P7,31

## ps7- fixed io- to- fpga regular io (ps7 gpio)

set_property  -dict {PACKAGE_PIN  K3      IOSTANDARD  LVCMOS18} [get_ports  IO_L11_SRCC_33_JX1_N]   ; ## U1,K3,IO_L11_SRCC_33_JX1_N,JX1,76,IO_L11_SRCC_33_JX1_N,P4,32 (U1,E26,PS_MIO00_500_JX4,JX4,97,PS_MIO00_500_JX4,P5,21)
set_property  -dict {PACKAGE_PIN  A9      IOSTANDARD  LVCMOS18} [get_ports  IO_L17_34_JX4_P]        ; ## U1,A9,IO_L17_34_JX4_P,JX4,67,IO_L17_34_JX4_P,P6,9            (U1,B20,PS_MIO51_501_JX4,JX4,100,PS_MIO51_501_JX4,P6,11)
set_property  -dict {PACKAGE_PIN  E5      IOSTANDARD  LVCMOS18} [get_ports  IO_L07_34_JX4_N]        ; ## U1,E5,IO_L07_34_JX4_N,JX4,37,IO_L07_34_JX4_N,P6,20           (U1,C24,PS_MIO15_500_JX4,JX4,85,PS_MIO15_500_JX4,P6,21)
set_property  -dict {PACKAGE_PIN  E6      IOSTANDARD  LVCMOS18} [get_ports  IO_L10_34_JX4_P]        ; ## U1,E6,IO_L10_34_JX4_P,JX4,42,IO_L10_34_JX4_P,P6,25           (U1,A25,PS_MIO10_500_JX4,JX4,87,PS_MIO10_500_JX4,P6,23)

## ps7- fixed io- to- ps7- fixed io (reference only)
## U1,B19,PS_MIO47_501_JX4,JX4,94,PS_MIO47_501_JX4,P7,24 == U1,E17,PS_MIO46_501_JX4,JX4,92,PS_MIO46_501_JX4,P7,22

## ps7- fixed io- orphans (reference only)
## U1,B25,PS_MIO13_500_JX4,JX4,91,PS_MIO13_500_JX4,P5,9
## U1,D23,PS_MIO14_500_JX4,JX4,93,PS_MIO14_500_JX4,P5,11
## U1,B26,PS_MIO11_500_JX4,JX4,88,PS_MIO11_500_JX4,P7,12

## fpga- regular io

set_property  -dict {PACKAGE_PIN  AA25    IOSTANDARD  LVCMOS25} [get_ports  IO_L01_13_JX2_P]        ; ## U1,AA25,IO_L01_13_JX2_P,JX2,1,IO_L01_13_JX2_P,P2,6
set_property  -dict {PACKAGE_PIN  AB26    IOSTANDARD  LVCMOS25} [get_ports  IO_L02_13_JX2_P]        ; ## U1,AB26,IO_L02_13_JX2_P,JX2,2,IO_L02_13_JX2_P,P2,5
set_property  -dict {PACKAGE_PIN  AB25    IOSTANDARD  LVCMOS25} [get_ports  IO_L01_13_JX2_N]        ; ## U1,AB25,IO_L01_13_JX2_N,JX2,3,IO_L01_13_JX2_N,P2,8
set_property  -dict {PACKAGE_PIN  AC26    IOSTANDARD  LVCMOS25} [get_ports  IO_L02_13_JX2_N]        ; ## U1,AC26,IO_L02_13_JX2_N,JX2,4,IO_L02_13_JX2_N,P2,7
set_property  -dict {PACKAGE_PIN  AE25    IOSTANDARD  LVCMOS25} [get_ports  IO_L03_13_JX2_P]        ; ## U1,AE25,IO_L03_13_JX2_P,JX2,5,IO_L03_13_JX2_P,P2,10
set_property  -dict {PACKAGE_PIN  AD25    IOSTANDARD  LVCMOS25} [get_ports  IO_L04_13_JX2_P]        ; ## U1,AD25,IO_L04_13_JX2_P,JX2,6,IO_L04_13_JX2_P,P2,9
set_property  -dict {PACKAGE_PIN  AE26    IOSTANDARD  LVCMOS25} [get_ports  IO_L03_13_JX2_N]        ; ## U1,AE26,IO_L03_13_JX2_N,JX2,7,IO_L03_13_JX2_N,P2,12
set_property  -dict {PACKAGE_PIN  AD26    IOSTANDARD  LVCMOS25} [get_ports  IO_L04_13_JX2_N]        ; ## U1,AD26,IO_L04_13_JX2_N,JX2,8,IO_L04_13_JX2_N,P2,11
set_property  -dict {PACKAGE_PIN  AE22    IOSTANDARD  LVCMOS25} [get_ports  IO_L07_13_JX2_P]        ; ## U1,AE22,IO_L07_13_JX2_P,JX2,23,IO_L07_13_JX2_P,P2,20
set_property  -dict {PACKAGE_PIN  AE23    IOSTANDARD  LVCMOS25} [get_ports  IO_L08_13_JX2_P]        ; ## U1,AE23,IO_L08_13_JX2_P,JX2,24,IO_L08_13_JX2_P,P2,19
set_property  -dict {PACKAGE_PIN  AF22    IOSTANDARD  LVCMOS25} [get_ports  IO_L07_13_JX2_N]        ; ## U1,AF22,IO_L07_13_JX2_N,JX2,25,IO_L07_13_JX2_N,P2,22
set_property  -dict {PACKAGE_PIN  AF23    IOSTANDARD  LVCMOS25} [get_ports  IO_L08_13_JX2_N]        ; ## U1,AF23,IO_L08_13_JX2_N,JX2,26,IO_L08_13_JX2_N,P2,21
set_property  -dict {PACKAGE_PIN  AB21    IOSTANDARD  LVCMOS25} [get_ports  IO_L09_13_JX2_P]        ; ## U1,AB21,IO_L09_13_JX2_P,JX2,29,IO_L09_13_JX2_P,P2,24
set_property  -dict {PACKAGE_PIN  AA22    IOSTANDARD  LVCMOS25} [get_ports  IO_L10_13_JX2_P]        ; ## U1,AA22,IO_L10_13_JX2_P,JX2,30,IO_L10_13_JX2_P,P2,23
set_property  -dict {PACKAGE_PIN  AB22    IOSTANDARD  LVCMOS25} [get_ports  IO_L09_13_JX2_N]        ; ## U1,AB22,IO_L09_13_JX2_N,JX2,31,IO_L09_13_JX2_N,P2,26
set_property  -dict {PACKAGE_PIN  AA23    IOSTANDARD  LVCMOS25} [get_ports  IO_L10_13_JX2_N]        ; ## U1,AA23,IO_L10_13_JX2_N,JX2,32,IO_L10_13_JX2_N,P2,25
set_property  -dict {PACKAGE_PIN  AD23    IOSTANDARD  LVCMOS25} [get_ports  IO_L11_SRCC_13_JX2_P]   ; ## U1,AD23,IO_L11_SRCC_13_JX2_P,JX2,35,IO_L11_SRCC_13_JX2_P,P2,28
set_property  -dict {PACKAGE_PIN  AC23    IOSTANDARD  LVCMOS25} [get_ports  IO_L12_MRCC_13_JX2_P]   ; ## U1,AC23,IO_L12_MRCC_13_JX2_P,JX2,36,IO_L12_MRCC_13_JX2_P,P2,27
set_property  -dict {PACKAGE_PIN  AD24    IOSTANDARD  LVCMOS25} [get_ports  IO_L11_SRCC_13_JX2_N]   ; ## U1,AD24,IO_L11_SRCC_13_JX2_N,JX2,37,IO_L11_SRCC_13_JX2_N,P2,30
set_property  -dict {PACKAGE_PIN  AC24    IOSTANDARD  LVCMOS25} [get_ports  IO_L12_MRCC_13_JX2_N]   ; ## U1,AC24,IO_L12_MRCC_13_JX2_N,JX2,38,IO_L12_MRCC_13_JX2_N,P2,29
set_property  -dict {PACKAGE_PIN  AD20    IOSTANDARD  LVCMOS25} [get_ports  IO_L13_MRCC_13_JX2_P]   ; ## U1,AD20,IO_L13_MRCC_13_JX2_P,JX2,41,IO_L13_MRCC_13_JX2_P,P2,32
set_property  -dict {PACKAGE_PIN  AC21    IOSTANDARD  LVCMOS25} [get_ports  IO_L14_SRCC_13_JX2_P]   ; ## U1,AC21,IO_L14_SRCC_13_JX2_P,JX2,42,IO_L14_SRCC_13_JX2_P,P2,31
set_property  -dict {PACKAGE_PIN  AD21    IOSTANDARD  LVCMOS25} [get_ports  IO_L13_MRCC_13_JX2_N]   ; ## U1,AD21,IO_L13_MRCC_13_JX2_N,JX2,43,IO_L13_MRCC_13_JX2_N,P2,34
set_property  -dict {PACKAGE_PIN  AC22    IOSTANDARD  LVCMOS25} [get_ports  IO_L14_SRCC_13_JX2_N]   ; ## U1,AC22,IO_L14_SRCC_13_JX2_N,JX2,44,IO_L14_SRCC_13_JX2_N,P2,33
set_property  -dict {PACKAGE_PIN  AF19    IOSTANDARD  LVCMOS25} [get_ports  IO_L15_13_JX2_P]        ; ## U1,AF19,IO_L15_13_JX2_P,JX2,47,IO_L15_13_JX2_P,P2,38
set_property  -dict {PACKAGE_PIN  AE20    IOSTANDARD  LVCMOS25} [get_ports  IO_L16_13_JX2_P]        ; ## U1,AE20,IO_L16_13_JX2_P,JX2,48,IO_L16_13_JX2_P,P2,37
set_property  -dict {PACKAGE_PIN  AF20    IOSTANDARD  LVCMOS25} [get_ports  IO_L15_13_JX2_N]        ; ## U1,AF20,IO_L15_13_JX2_N,JX2,49,IO_L15_13_JX2_N,P2,40
set_property  -dict {PACKAGE_PIN  AE21    IOSTANDARD  LVCMOS25} [get_ports  IO_L16_13_JX2_N]        ; ## U1,AE21,IO_L16_13_JX2_N,JX2,50,IO_L16_13_JX2_N,P2,39
set_property  -dict {PACKAGE_PIN  AD18    IOSTANDARD  LVCMOS25} [get_ports  IO_L17_13_JX2_P]        ; ## U1,AD18,IO_L17_13_JX2_P,JX2,53,IO_L17_13_JX2_P,P2,42
set_property  -dict {PACKAGE_PIN  AE18    IOSTANDARD  LVCMOS25} [get_ports  IO_L18_13_JX2_P]        ; ## U1,AE18,IO_L18_13_JX2_P,JX2,54,IO_L18_13_JX2_P,P2,41
set_property  -dict {PACKAGE_PIN  AD19    IOSTANDARD  LVCMOS25} [get_ports  IO_L17_13_JX2_N]        ; ## U1,AD19,IO_L17_13_JX2_N,JX2,55,IO_L17_13_JX2_N,P2,44
set_property  -dict {PACKAGE_PIN  AF18    IOSTANDARD  LVCMOS25} [get_ports  IO_L18_13_JX2_N]        ; ## U1,AF18,IO_L18_13_JX2_N,JX2,56,IO_L18_13_JX2_N,P2,43
set_property  -dict {PACKAGE_PIN  W20     IOSTANDARD  LVCMOS25} [get_ports  IO_L19_13_JX2_P]        ; ## U1,W20,IO_L19_13_JX2_P,JX2,61,IO_L19_13_JX2_P,P2,46
set_property  -dict {PACKAGE_PIN  AA20    IOSTANDARD  LVCMOS25} [get_ports  IO_L20_13_JX2_P]        ; ## U1,AA20,IO_L20_13_JX2_P,JX2,62,IO_L20_13_JX2_P,P2,45
set_property  -dict {PACKAGE_PIN  Y20     IOSTANDARD  LVCMOS25} [get_ports  IO_L19_13_JX2_N]        ; ## U1,Y20,IO_L19_13_JX2_N,JX2,63,IO_L19_13_JX2_N,P2,48
set_property  -dict {PACKAGE_PIN  AB20    IOSTANDARD  LVCMOS25} [get_ports  IO_L20_13_JX2_N]        ; ## U1,AB20,IO_L20_13_JX2_N,JX2,64,IO_L20_13_JX2_N,P2,47
set_property  -dict {PACKAGE_PIN  AC18    IOSTANDARD  LVCMOS25} [get_ports  IO_L21_13_JX2_P]        ; ## U1,AC18,IO_L21_13_JX2_P,JX2,67,IO_L21_13_JX2_P,P2,52
set_property  -dict {PACKAGE_PIN  AA19    IOSTANDARD  LVCMOS25} [get_ports  IO_L22_13_JX2_P]        ; ## U1,AA19,IO_L22_13_JX2_P,JX2,68,IO_L22_13_JX2_P,P2,51
set_property  -dict {PACKAGE_PIN  AC19    IOSTANDARD  LVCMOS25} [get_ports  IO_L21_13_JX2_N]        ; ## U1,AC19,IO_L21_13_JX2_N,JX2,69,IO_L21_13_JX2_N,P2,54
set_property  -dict {PACKAGE_PIN  AB19    IOSTANDARD  LVCMOS25} [get_ports  IO_L22_13_JX2_N]        ; ## U1,AB19,IO_L22_13_JX2_N,JX2,70,IO_L22_13_JX2_N,P2,53
set_property  -dict {PACKAGE_PIN  W18     IOSTANDARD  LVCMOS25} [get_ports  IO_L23_13_JX2_P]        ; ## U1,W18,IO_L23_13_JX2_P,JX2,73,IO_L23_13_JX2_P,P2,56
set_property  -dict {PACKAGE_PIN  Y18     IOSTANDARD  LVCMOS25} [get_ports  IO_L24_13_JX2_P]        ; ## U1,Y18,IO_L24_13_JX2_P,JX2,74,IO_L24_13_JX2_P,P2,55
set_property  -dict {PACKAGE_PIN  W19     IOSTANDARD  LVCMOS25} [get_ports  IO_L23_13_JX2_N]        ; ## U1,W19,IO_L23_13_JX2_N,JX2,75,IO_L23_13_JX2_N,P2,58
set_property  -dict {PACKAGE_PIN  AA18    IOSTANDARD  LVCMOS25} [get_ports  IO_L24_13_JX2_N]        ; ## U1,AA18,IO_L24_13_JX2_N,JX2,76,IO_L24_13_JX2_N,P2,57
set_property  -dict {PACKAGE_PIN  G4      IOSTANDARD  LVCMOS18} [get_ports  IO_L01_33_JX1_P]        ; ## U1,G4,IO_L01_33_JX1_P,JX1,35,IO_L01_33_JX1_P,P4,2
set_property  -dict {PACKAGE_PIN  D4      IOSTANDARD  LVCMOS18} [get_ports  IO_L02_33_JX1_P]        ; ## U1,D4,IO_L02_33_JX1_P,JX1,41,IO_L02_33_JX1_P,P4,1
set_property  -dict {PACKAGE_PIN  F4      IOSTANDARD  LVCMOS18} [get_ports  IO_L01_33_JX1_N]        ; ## U1,F4,IO_L01_33_JX1_N,JX1,37,IO_L01_33_JX1_N,P4,4
set_property  -dict {PACKAGE_PIN  D3      IOSTANDARD  LVCMOS18} [get_ports  IO_L02_33_JX1_N]        ; ## U1,D3,IO_L02_33_JX1_N,JX1,43,IO_L02_33_JX1_N,P4,3
set_property  -dict {PACKAGE_PIN  G2      IOSTANDARD  LVCMOS18} [get_ports  IO_L03_33_JX1_P]        ; ## U1,G2,IO_L03_33_JX1_P,JX1,42,IO_L03_33_JX1_P,P4,6
set_property  -dict {PACKAGE_PIN  D1      IOSTANDARD  LVCMOS18} [get_ports  IO_L04_33_JX1_P]        ; ## U1,D1,IO_L04_33_JX1_P,JX1,47,IO_L04_33_JX1_P,P4,5
set_property  -dict {PACKAGE_PIN  F2      IOSTANDARD  LVCMOS18} [get_ports  IO_L03_33_JX1_N]        ; ## U1,F2,IO_L03_33_JX1_N,JX1,44,IO_L03_33_JX1_N,P4,8
set_property  -dict {PACKAGE_PIN  C1      IOSTANDARD  LVCMOS18} [get_ports  IO_L04_33_JX1_N]        ; ## U1,C1,IO_L04_33_JX1_N,JX1,49,IO_L04_33_JX1_N,P4,7
set_property  -dict {PACKAGE_PIN  E2      IOSTANDARD  LVCMOS18} [get_ports  IO_L05_33_JX1_P]        ; ## U1,E2,IO_L05_33_JX1_P,JX1,54,IO_L05_33_JX1_P,P4,14
set_property  -dict {PACKAGE_PIN  F3      IOSTANDARD  LVCMOS18} [get_ports  IO_L06_33_JX1_P]        ; ## U1,F3,IO_L06_33_JX1_P,JX1,61,IO_L06_33_JX1_P,P4,13
set_property  -dict {PACKAGE_PIN  E1      IOSTANDARD  LVCMOS18} [get_ports  IO_L05_33_JX1_N]        ; ## U1,E1,IO_L05_33_JX1_N,JX1,56,IO_L05_33_JX1_N,P4,16
set_property  -dict {PACKAGE_PIN  E3      IOSTANDARD  LVCMOS18} [get_ports  IO_L06_33_JX1_N]        ; ## U1,E3,IO_L06_33_JX1_N,JX1,63,IO_L06_33_JX1_N,P4,15
set_property  -dict {PACKAGE_PIN  J1      IOSTANDARD  LVCMOS18} [get_ports  IO_L07_33_JX1_P]        ; ## U1,J1,IO_L07_33_JX1_P,JX1,62,IO_L07_33_JX1_P,P4,18
set_property  -dict {PACKAGE_PIN  H4      IOSTANDARD  LVCMOS18} [get_ports  IO_L08_33_JX1_P]        ; ## U1,H4,IO_L08_33_JX1_P,JX1,67,IO_L08_33_JX1_P,P4,17
set_property  -dict {PACKAGE_PIN  H1      IOSTANDARD  LVCMOS18} [get_ports  IO_L07_33_JX1_N]        ; ## U1,H1,IO_L07_33_JX1_N,JX1,64,IO_L07_33_JX1_N,P4,20
set_property  -dict {PACKAGE_PIN  H3      IOSTANDARD  LVCMOS18} [get_ports  IO_L08_33_JX1_N]        ; ## U1,H3,IO_L08_33_JX1_N,JX1,69,IO_L08_33_JX1_N,P4,19
set_property  -dict {PACKAGE_PIN  K2      IOSTANDARD  LVCMOS18} [get_ports  IO_L09_33_JX1_P]        ; ## U1,K2,IO_L09_33_JX1_P,JX1,68,IO_L09_33_JX1_P,P4,26
set_property  -dict {PACKAGE_PIN  H2      IOSTANDARD  LVCMOS18} [get_ports  IO_L10_33_JX1_P]        ; ## U1,H2,IO_L10_33_JX1_P,JX1,73,IO_L10_33_JX1_P,P4,25
set_property  -dict {PACKAGE_PIN  K1      IOSTANDARD  LVCMOS18} [get_ports  IO_L09_33_JX1_N]        ; ## U1,K1,IO_L09_33_JX1_N,JX1,70,IO_L09_33_JX1_N,P4,28
set_property  -dict {PACKAGE_PIN  G1      IOSTANDARD  LVCMOS18} [get_ports  IO_L10_33_JX1_N]        ; ## U1,G1,IO_L10_33_JX1_N,JX1,75,IO_L10_33_JX1_N,P4,27
set_property  -dict {PACKAGE_PIN  L3      IOSTANDARD  LVCMOS18} [get_ports  IO_L11_SRCC_33_JX1_P]   ; ## U1,L3,IO_L11_SRCC_33_JX1_P,JX1,74,IO_L11_SRCC_33_JX1_P,P4,30
set_property  -dict {PACKAGE_PIN  J4      IOSTANDARD  LVCMOS18} [get_ports  IO_L12_MRCC_33_JX1_P]   ; ## U1,J4,IO_L12_MRCC_33_JX1_P,JX1,81,IO_L12_MRCC_33_JX1_P,P4,29
set_property  -dict {PACKAGE_PIN  M2      IOSTANDARD  LVCMOS18} [get_ports  IO_L16_33_JX1_P]        ; ## U1,M2,IO_L16_33_JX1_P,JX1,11,IO_L16_33_JX1_P,P5,2
set_property  -dict {PACKAGE_PIN  N4      IOSTANDARD  LVCMOS18} [get_ports  IO_L17_33_JX1_P]        ; ## U1,N4,IO_L17_33_JX1_P,JX1,12,IO_L17_33_JX1_P,P5,1
set_property  -dict {PACKAGE_PIN  L2      IOSTANDARD  LVCMOS18} [get_ports  IO_L16_33_JX1_N]        ; ## U1,L2,IO_L16_33_JX1_N,JX1,13,IO_L16_33_JX1_N,P5,4
set_property  -dict {PACKAGE_PIN  M4      IOSTANDARD  LVCMOS18} [get_ports  IO_L17_33_JX1_N]        ; ## U1,M4,IO_L17_33_JX1_N,JX1,14,IO_L17_33_JX1_N,P5,3
set_property  -dict {PACKAGE_PIN  N1      IOSTANDARD  LVCMOS18} [get_ports  IO_L18_33_JX1_P]        ; ## U1,N1,IO_L18_33_JX1_P,JX1,17,IO_L18_33_JX1_P,P5,6
set_property  -dict {PACKAGE_PIN  M7      IOSTANDARD  LVCMOS18} [get_ports  IO_L19_33_JX1_P]        ; ## U1,M7,IO_L19_33_JX1_P,JX1,18,IO_L19_33_JX1_P,P5,5
set_property  -dict {PACKAGE_PIN  M1      IOSTANDARD  LVCMOS18} [get_ports  IO_L18_33_JX1_N]        ; ## U1,M1,IO_L18_33_JX1_N,JX1,19,IO_L18_33_JX1_N,P5,8
set_property  -dict {PACKAGE_PIN  L7      IOSTANDARD  LVCMOS18} [get_ports  IO_L19_33_JX1_N]        ; ## U1,L7,IO_L19_33_JX1_N,JX1,20,IO_L19_33_JX1_N,P5,7
set_property  -dict {PACKAGE_PIN  M8      IOSTANDARD  LVCMOS18} [get_ports  IO_L21_33_JX1_P]        ; ## U1,M8,IO_L21_33_JX1_P,JX1,24,IO_L21_33_JX1_P,P5,14
set_property  -dict {PACKAGE_PIN  K5      IOSTANDARD  LVCMOS18} [get_ports  IO_L20_33_JX1_P]        ; ## U1,K5,IO_L20_33_JX1_P,JX1,23,IO_L20_33_JX1_P,P5,13
set_property  -dict {PACKAGE_PIN  L8      IOSTANDARD  LVCMOS18} [get_ports  IO_L21_33_JX1_N]        ; ## U1,L8,IO_L21_33_JX1_N,JX1,26,IO_L21_33_JX1_N,P5,16
set_property  -dict {PACKAGE_PIN  J5      IOSTANDARD  LVCMOS18} [get_ports  IO_L20_33_JX1_N]        ; ## U1,J5,IO_L20_33_JX1_N,JX1,25,IO_L20_33_JX1_N,P5,15
set_property  -dict {PACKAGE_PIN  N7      IOSTANDARD  LVCMOS18} [get_ports  IO_L23_33_JX1_P]        ; ## U1,N7,IO_L23_33_JX1_P,JX1,30,IO_L23_33_JX1_P,P5,18
set_property  -dict {PACKAGE_PIN  K6      IOSTANDARD  LVCMOS18} [get_ports  IO_L22_33_JX1_P]        ; ## U1,K6,IO_L22_33_JX1_P,JX1,29,IO_L22_33_JX1_P,P5,17
set_property  -dict {PACKAGE_PIN  N6      IOSTANDARD  LVCMOS18} [get_ports  IO_L23_33_JX1_N]        ; ## U1,N6,IO_L23_33_JX1_N,JX1,32,IO_L23_33_JX1_N,P5,20
set_property  -dict {PACKAGE_PIN  J6      IOSTANDARD  LVCMOS18} [get_ports  IO_L22_33_JX1_N]        ; ## U1,J6,IO_L22_33_JX1_N,JX1,31,IO_L22_33_JX1_N,P5,19
set_property  -dict {PACKAGE_PIN  D5      IOSTANDARD  LVCMOS18} [get_ports  IO_L10_34_JX4_N]        ; ## U1,D5,IO_L10_34_JX4_N,JX4,44,IO_L10_34_JX4_N,P6,27
set_property  -dict {PACKAGE_PIN  K10     IOSTANDARD  LVCMOS18} [get_ports  IO_25_34_JX4]           ; ## U1,K10,IO_25_34_JX4,JX4,64,IO_25_34_JX4,P5,23
set_property  -dict {PACKAGE_PIN  K8      IOSTANDARD  LVCMOS18} [get_ports  IO_L24_33_JX1_P]        ; ## U1,K8,IO_L24_33_JX1_P,JX1,36,IO_L24_33_JX1_P,P5,26
set_property  -dict {PACKAGE_PIN  L5      IOSTANDARD  LVCMOS18} [get_ports  IO_L14_SRCC_33_JX1_P]   ; ## U1,L5,IO_L14_SRCC_33_JX1_P,JX1,48,IO_L14_SRCC_33_JX1_P,P5,25
set_property  -dict {PACKAGE_PIN  K7      IOSTANDARD  LVCMOS18} [get_ports  IO_L24_33_JX1_N]        ; ## U1,K7,IO_L24_33_JX1_N,JX1,38,IO_L24_33_JX1_N,P5,28
set_property  -dict {PACKAGE_PIN  L4      IOSTANDARD  LVCMOS18} [get_ports  IO_L14_SRCC_33_JX1_N]   ; ## U1,L4,IO_L14_SRCC_33_JX1_N,JX1,50,IO_L14_SRCC_33_JX1_N,P5,27
set_property  -dict {PACKAGE_PIN  N3      IOSTANDARD  LVCMOS18} [get_ports  IO_L15_33_JX1_P]        ; ## U1,N3,IO_L15_33_JX1_P,JX1,53,IO_L15_33_JX1_P,P5,30
set_property  -dict {PACKAGE_PIN  M6      IOSTANDARD  LVCMOS18} [get_ports  IO_L13_MRCC_33_JX1_P]   ; ## U1,M6,IO_L13_MRCC_33_JX1_P,JX1,82,IO_L13_MRCC_33_JX1_P,P5,29
set_property  -dict {PACKAGE_PIN  N2      IOSTANDARD  LVCMOS18} [get_ports  IO_L15_33_JX1_N]        ; ## U1,N2,IO_L15_33_JX1_N,JX1,55,IO_L15_33_JX1_N,P5,32
set_property  -dict {PACKAGE_PIN  M5      IOSTANDARD  LVCMOS18} [get_ports  IO_L13_MRCC_33_JX1_N]   ; ## U1,M5,IO_L13_MRCC_33_JX1_N,JX1,84,IO_L13_MRCC_33_JX1_N,P5,31
set_property  -dict {PACKAGE_PIN  J11     IOSTANDARD  LVCMOS18} [get_ports  IO_L01_34_JX4_P]        ; ## U1,J11,IO_L01_34_JX4_P,JX4,19,IO_L01_34_JX4_P,P6,2
set_property  -dict {PACKAGE_PIN  G6      IOSTANDARD  LVCMOS18} [get_ports  IO_L02_34_JX4_P]        ; ## U1,G6,IO_L02_34_JX4_P,JX4,20,IO_L02_34_JX4_P,P6,1
set_property  -dict {PACKAGE_PIN  H11     IOSTANDARD  LVCMOS18} [get_ports  IO_L01_34_JX4_N]        ; ## U1,H11,IO_L01_34_JX4_N,JX4,21,IO_L01_34_JX4_N,P6,4
set_property  -dict {PACKAGE_PIN  G5      IOSTANDARD  LVCMOS18} [get_ports  IO_L02_34_JX4_N]        ; ## U1,G5,IO_L02_34_JX4_N,JX4,22,IO_L02_34_JX4_N,P6,3
set_property  -dict {PACKAGE_PIN  H9      IOSTANDARD  LVCMOS18} [get_ports  IO_L03_34_JX4_P]        ; ## U1,H9,IO_L03_34_JX4_P,JX4,25,IO_L03_34_JX4_P,P6,6
set_property  -dict {PACKAGE_PIN  H7      IOSTANDARD  LVCMOS18} [get_ports  IO_L04_34_JX4_P]        ; ## U1,H7,IO_L04_34_JX4_P,JX4,26,IO_L04_34_JX4_P,P6,5
set_property  -dict {PACKAGE_PIN  G9      IOSTANDARD  LVCMOS18} [get_ports  IO_L03_34_JX4_N]        ; ## U1,G9,IO_L03_34_JX4_N,JX4,27,IO_L03_34_JX4_N,P6,8
set_property  -dict {PACKAGE_PIN  H6      IOSTANDARD  LVCMOS18} [get_ports  IO_L04_34_JX4_N]        ; ## U1,H6,IO_L04_34_JX4_N,JX4,28,IO_L04_34_JX4_N,P6,7
set_property  -dict {PACKAGE_PIN  J10     IOSTANDARD  LVCMOS18} [get_ports  IO_L05_34_JX4_P]        ; ## U1,J10,IO_L05_34_JX4_P,JX4,31,IO_L05_34_JX4_P,P6,14
set_property  -dict {PACKAGE_PIN  J8      IOSTANDARD  LVCMOS18} [get_ports  IO_L06_34_JX4_P]        ; ## U1,J8,IO_L06_34_JX4_P,JX4,32,IO_L06_34_JX4_P,P6,13
set_property  -dict {PACKAGE_PIN  J9      IOSTANDARD  LVCMOS18} [get_ports  IO_L05_34_JX4_N]        ; ## U1,J9,IO_L05_34_JX4_N,JX4,33,IO_L05_34_JX4_N,P6,16
set_property  -dict {PACKAGE_PIN  H8      IOSTANDARD  LVCMOS18} [get_ports  IO_L06_34_JX4_N]        ; ## U1,H8,IO_L06_34_JX4_N,JX4,34,IO_L06_34_JX4_N,P6,15
set_property  -dict {PACKAGE_PIN  F5      IOSTANDARD  LVCMOS18} [get_ports  IO_L07_34_JX4_P]        ; ## U1,F5,IO_L07_34_JX4_P,JX4,35,IO_L07_34_JX4_P,P6,18
set_property  -dict {PACKAGE_PIN  D9      IOSTANDARD  LVCMOS18} [get_ports  IO_L08_34_JX4_P]        ; ## U1,D9,IO_L08_34_JX4_P,JX4,36,IO_L08_34_JX4_P,P6,17
set_property  -dict {PACKAGE_PIN  F8      IOSTANDARD  LVCMOS18} [get_ports  IO_L11_SRCC_34_JX4_P]   ; ## U1,F8,IO_L11_SRCC_34_JX4_P,JX4,45,IO_L11_SRCC_34_JX4_P,P6,30
set_property  -dict {PACKAGE_PIN  G7      IOSTANDARD  LVCMOS18} [get_ports  IO_L12_MRCC_34_JX4_P]   ; ## U1,G7,IO_L12_MRCC_34_JX4_P,JX4,46,IO_L12_MRCC_34_JX4_P,P6,29
set_property  -dict {PACKAGE_PIN  E7      IOSTANDARD  LVCMOS18} [get_ports  IO_L11_SRCC_34_JX4_N]   ; ## U1,E7,IO_L11_SRCC_34_JX4_N,JX4,47,IO_L11_SRCC_34_JX4_N,P6,32
set_property  -dict {PACKAGE_PIN  F7      IOSTANDARD  LVCMOS18} [get_ports  IO_L12_MRCC_34_JX4_N]   ; ## U1,F7,IO_L12_MRCC_34_JX4_N,JX4,48,IO_L12_MRCC_34_JX4_N,P6,31
set_property  -dict {PACKAGE_PIN  C8      IOSTANDARD  LVCMOS18} [get_ports  IO_L13_MRCC_34_JX4_P]   ; ## U1,C8,IO_L13_MRCC_34_JX4_P,JX4,51,IO_L13_MRCC_34_JX4_P,P7,2
set_property  -dict {PACKAGE_PIN  D6      IOSTANDARD  LVCMOS18} [get_ports  IO_L14_SRCC_34_JX4_P]   ; ## U1,D6,IO_L14_SRCC_34_JX4_P,JX4,52,IO_L14_SRCC_34_JX4_P,P7,1
set_property  -dict {PACKAGE_PIN  C7      IOSTANDARD  LVCMOS18} [get_ports  IO_L13_MRCC_34_JX4_N]   ; ## U1,C7,IO_L13_MRCC_34_JX4_N,JX4,53,IO_L13_MRCC_34_JX4_N,P7,4
set_property  -dict {PACKAGE_PIN  C6      IOSTANDARD  LVCMOS18} [get_ports  IO_L14_SRCC_34_JX4_N]   ; ## U1,C6,IO_L14_SRCC_34_JX4_N,JX4,54,IO_L14_SRCC_34_JX4_N,P7,3
set_property  -dict {PACKAGE_PIN  C9      IOSTANDARD  LVCMOS18} [get_ports  IO_L15_34_JX4_P]        ; ## U1,C9,IO_L15_34_JX4_P,JX4,57,IO_L15_34_JX4_P,P7,6
set_property  -dict {PACKAGE_PIN  B10     IOSTANDARD  LVCMOS18} [get_ports  IO_L16_34_JX4_P]        ; ## U1,B10,IO_L16_34_JX4_P,JX4,58,IO_L16_34_JX4_P,P7,5
set_property  -dict {PACKAGE_PIN  B9      IOSTANDARD  LVCMOS18} [get_ports  IO_L15_34_JX4_N]        ; ## U1,B9,IO_L15_34_JX4_N,JX4,59,IO_L15_34_JX4_N,P7,8
set_property  -dict {PACKAGE_PIN  A10     IOSTANDARD  LVCMOS18} [get_ports  IO_L16_34_JX4_N]        ; ## U1,A10,IO_L16_34_JX4_N,JX4,60,IO_L16_34_JX4_N,P7,7
set_property  -dict {PACKAGE_PIN  C4      IOSTANDARD  LVCMOS18} [get_ports  IO_L19_34_JX4_P]        ; ## U1,C4,IO_L19_34_JX4_P,JX4,73,IO_L19_34_JX4_P,P7,18
set_property  -dict {PACKAGE_PIN  B5      IOSTANDARD  LVCMOS18} [get_ports  IO_L20_34_JX4_P]        ; ## U1,B5,IO_L20_34_JX4_P,JX4,74,IO_L20_34_JX4_P,P7,17
set_property  -dict {PACKAGE_PIN  C3      IOSTANDARD  LVCMOS18} [get_ports  IO_L19_34_JX4_N]        ; ## U1,C3,IO_L19_34_JX4_N,JX4,75,IO_L19_34_JX4_N,P7,20
set_property  -dict {PACKAGE_PIN  B4      IOSTANDARD  LVCMOS18} [get_ports  IO_L20_34_JX4_N]        ; ## U1,B4,IO_L20_34_JX4_N,JX4,76,IO_L20_34_JX4_N,P7,19
set_property  -dict {PACKAGE_PIN  B6      IOSTANDARD  LVCMOS18} [get_ports  IO_L21_34_JX4_P]        ; ## U1,B6,IO_L21_34_JX4_P,JX4,77,IO_L21_34_JX4_P,P7,26
set_property  -dict {PACKAGE_PIN  A4      IOSTANDARD  LVCMOS18} [get_ports  IO_L22_34_JX4_P]        ; ## U1,A4,IO_L22_34_JX4_P,JX4,78,IO_L22_34_JX4_P,P7,25
set_property  -dict {PACKAGE_PIN  A5      IOSTANDARD  LVCMOS18} [get_ports  IO_L21_34_JX4_N]        ; ## U1,A5,IO_L21_34_JX4_N,JX4,79,IO_L21_34_JX4_N,P7,28
set_property  -dict {PACKAGE_PIN  A3      IOSTANDARD  LVCMOS18} [get_ports  IO_L22_34_JX4_N]        ; ## U1,A3,IO_L22_34_JX4_N,JX4,80,IO_L22_34_JX4_N,P7,27
set_property  -dict {PACKAGE_PIN  B7      IOSTANDARD  LVCMOS18} [get_ports  IO_L18_34_JX4_P]        ; ## U1,B7,IO_L18_34_JX4_P,JX4,68,IO_L18_34_JX4_P,P7,30
set_property  -dict {PACKAGE_PIN  L9      IOSTANDARD  LVCMOS18} [get_ports  IO_00_33_JX1]           ; ## U1,L9,IO_00_33_JX1,JX1,9,IO_00_33_JX1,P7,29
set_property  -dict {PACKAGE_PIN  AD15    IOSTANDARD  LVCMOS25} [get_ports  IO_L15_12_JX3_N]        ; ## U1,AD15,IO_L15_12_JX3_N,JX3,99,IO_L15_12_JX3_N,P13,6
set_property  -dict {PACKAGE_PIN  AF14    IOSTANDARD  LVCMOS25} [get_ports  IO_L16_12_JX3_N]        ; ## U1,AF14,IO_L16_12_JX3_N,JX3,100,IO_L16_12_JX3_N,P13,5
set_property  -dict {PACKAGE_PIN  AD16    IOSTANDARD  LVCMOS25} [get_ports  IO_L15_12_JX3_P]        ; ## U1,AD16,IO_L15_12_JX3_P,JX3,97,IO_L15_12_JX3_P,P13,8
set_property  -dict {PACKAGE_PIN  AF15    IOSTANDARD  LVCMOS25} [get_ports  IO_L16_12_JX3_P]        ; ## U1,AF15,IO_L16_12_JX3_P,JX3,98,IO_L16_12_JX3_P,P13,7
set_property  -dict {PACKAGE_PIN  AD14    IOSTANDARD  LVCMOS25} [get_ports  IO_L13_MRCC_12_JX3_N]   ; ## U1,AD14,IO_L13_MRCC_12_JX3_N,JX3,93,IO_L13_MRCC_12_JX3_N,P13,10
set_property  -dict {PACKAGE_PIN  AB14    IOSTANDARD  LVCMOS25} [get_ports  IO_L14_SRCC_12_JX3_N]   ; ## U1,AB14,IO_L14_SRCC_12_JX3_N,JX3,94,IO_L14_SRCC_12_JX3_N,P13,9
set_property  -dict {PACKAGE_PIN  AC14    IOSTANDARD  LVCMOS25} [get_ports  IO_L13_MRCC_12_JX3_P]   ; ## U1,AC14,IO_L13_MRCC_12_JX3_P,JX3,91,IO_L13_MRCC_12_JX3_P,P13,12
set_property  -dict {PACKAGE_PIN  AB15    IOSTANDARD  LVCMOS25} [get_ports  IO_L14_SRCC_12_JX3_P]   ; ## U1,AB15,IO_L14_SRCC_12_JX3_P,JX3,92,IO_L14_SRCC_12_JX3_P,P13,11
set_property  -dict {PACKAGE_PIN  AD11    IOSTANDARD  LVCMOS25} [get_ports  IO_L11_SRCC_12_JX3_N]   ; ## U1,AD11,IO_L11_SRCC_12_JX3_N,JX3,87,IO_L11_SRCC_12_JX3_N,P13,14
set_property  -dict {PACKAGE_PIN  AD13    IOSTANDARD  LVCMOS25} [get_ports  IO_L12_MRCC_12_JX3_N]   ; ## U1,AD13,IO_L12_MRCC_12_JX3_N,JX3,88,IO_L12_MRCC_12_JX3_N,P13,13
set_property  -dict {PACKAGE_PIN  AC12    IOSTANDARD  LVCMOS25} [get_ports  IO_L11_SRCC_12_JX3_P]   ; ## U1,AC12,IO_L11_SRCC_12_JX3_P,JX3,85,IO_L11_SRCC_12_JX3_P,P13,16
set_property  -dict {PACKAGE_PIN  AC13    IOSTANDARD  LVCMOS25} [get_ports  IO_L12_MRCC_12_JX3_P]   ; ## U1,AC13,IO_L12_MRCC_12_JX3_P,JX3,86,IO_L12_MRCC_12_JX3_P,P13,15
set_property  -dict {PACKAGE_PIN  AF10    IOSTANDARD  LVCMOS25} [get_ports  IO_L09_12_JX3_N]        ; ## U1,AF10,IO_L09_12_JX3_N,JX3,81,IO_L09_12_JX3_N,P13,20
set_property  -dict {PACKAGE_PIN  AF13    IOSTANDARD  LVCMOS25} [get_ports  IO_L10_12_JX3_N]        ; ## U1,AF13,IO_L10_12_JX3_N,JX3,82,IO_L10_12_JX3_N,P13,19
set_property  -dict {PACKAGE_PIN  AE11    IOSTANDARD  LVCMOS25} [get_ports  IO_L09_12_JX3_P]        ; ## U1,AE11,IO_L09_12_JX3_P,JX3,79,IO_L09_12_JX3_P,P13,22
set_property  -dict {PACKAGE_PIN  AE13    IOSTANDARD  LVCMOS25} [get_ports  IO_L10_12_JX3_P]        ; ## U1,AE13,IO_L10_12_JX3_P,JX3,80,IO_L10_12_JX3_P,P13,21
set_property  -dict {PACKAGE_PIN  AD10    IOSTANDARD  LVCMOS25} [get_ports  IO_L07_12_JX3_N]        ; ## U1,AD10,IO_L07_12_JX3_N,JX3,75,IO_L07_12_JX3_N,P13,24
set_property  -dict {PACKAGE_PIN  AF12    IOSTANDARD  LVCMOS25} [get_ports  IO_L08_12_JX3_N]        ; ## U1,AF12,IO_L08_12_JX3_N,JX3,76,IO_L08_12_JX3_N,P13,23
set_property  -dict {PACKAGE_PIN  AE10    IOSTANDARD  LVCMOS25} [get_ports  IO_L07_12_JX3_P]        ; ## U1,AE10,IO_L07_12_JX3_P,JX3,73,IO_L07_12_JX3_P,P13,26
set_property  -dict {PACKAGE_PIN  AE12    IOSTANDARD  LVCMOS25} [get_ports  IO_L08_12_JX3_P]        ; ## U1,AE12,IO_L08_12_JX3_P,JX3,74,IO_L08_12_JX3_P,P13,25
set_property  -dict {PACKAGE_PIN  Y13     IOSTANDARD  LVCMOS25} [get_ports  IO_L05_12_JX3_N]        ; ## U1,Y13,IO_L05_12_JX3_N,JX3,44,IO_L05_12_JX3_N,P13,28
set_property  -dict {PACKAGE_PIN  AA12    IOSTANDARD  LVCMOS25} [get_ports  IO_L06_12_JX3_N]        ; ## U1,AA12,IO_L06_12_JX3_N,JX3,66,IO_L06_12_JX3_N,P13,27
set_property  -dict {PACKAGE_PIN  W13     IOSTANDARD  LVCMOS25} [get_ports  IO_L05_12_JX3_P]        ; ## U1,W13,IO_L05_12_JX3_P,JX3,42,IO_L05_12_JX3_P,P13,30
set_property  -dict {PACKAGE_PIN  AA13    IOSTANDARD  LVCMOS25} [get_ports  IO_L06_12_JX3_P]        ; ## U1,AA13,IO_L06_12_JX3_P,JX3,64,IO_L06_12_JX3_P,P13,29
set_property  -dict {PACKAGE_PIN  AA10    IOSTANDARD  LVCMOS25} [get_ports  IO_L03_12_JX3_N]        ; ## U1,AA10,IO_L03_12_JX3_N,JX3,28,IO_L03_12_JX3_N,P13,32
set_property  -dict {PACKAGE_PIN  AB10    IOSTANDARD  LVCMOS25} [get_ports  IO_L04_12_JX3_N]        ; ## U1,AB10,IO_L04_12_JX3_N,JX3,33,IO_L04_12_JX3_N,P13,31
set_property  -dict {PACKAGE_PIN  Y10     IOSTANDARD  LVCMOS25} [get_ports  IO_L03_12_JX3_P]        ; ## U1,Y10,IO_L03_12_JX3_P,JX3,26,IO_L03_12_JX3_P,P13,34
set_property  -dict {PACKAGE_PIN  AB11    IOSTANDARD  LVCMOS25} [get_ports  IO_L04_12_JX3_P]        ; ## U1,AB11,IO_L04_12_JX3_P,JX3,31,IO_L04_12_JX3_P,P13,33
set_property  -dict {PACKAGE_PIN  Y11     IOSTANDARD  LVCMOS25} [get_ports  IO_L01_12_JX3_N]        ; ## U1,Y11,IO_L01_12_JX3_N,JX3,22,IO_L01_12_JX3_N,P13,36
set_property  -dict {PACKAGE_PIN  AC11    IOSTANDARD  LVCMOS25} [get_ports  IO_L02_12_JX3_N]        ; ## U1,AC11,IO_L02_12_JX3_N,JX3,27,IO_L02_12_JX3_N,P13,35
set_property  -dict {PACKAGE_PIN  Y12     IOSTANDARD  LVCMOS25} [get_ports  IO_L01_12_JX3_P]        ; ## U1,Y12,IO_L01_12_JX3_P,JX3,20,IO_L01_12_JX3_P,P13,38
set_property  -dict {PACKAGE_PIN  AB12    IOSTANDARD  LVCMOS25} [get_ports  IO_L02_12_JX3_P]        ; ## U1,AB12,IO_L02_12_JX3_P,JX3,25,IO_L02_12_JX3_P,P13,37
set_property  -dict {PACKAGE_PIN  AE16    IOSTANDARD  LVCMOS25} [get_ports  IO_L17_12_JX2_P]        ; ## U1,AE16,IO_L17_12_JX2_P,JX2,82,IO_L17_12_JX2_P,P13,42
set_property  -dict {PACKAGE_PIN  AE17    IOSTANDARD  LVCMOS25} [get_ports  IO_L18_12_JX2_P]        ; ## U1,AE17,IO_L18_12_JX2_P,JX2,81,IO_L18_12_JX2_P,P13,41
set_property  -dict {PACKAGE_PIN  AE15    IOSTANDARD  LVCMOS25} [get_ports  IO_L17_12_JX2_N]        ; ## U1,AE15,IO_L17_12_JX2_N,JX2,84,IO_L17_12_JX2_N,P13,44
set_property  -dict {PACKAGE_PIN  AF17    IOSTANDARD  LVCMOS25} [get_ports  IO_L18_12_JX2_N]        ; ## U1,AF17,IO_L18_12_JX2_N,JX2,83,IO_L18_12_JX2_N,P13,43
set_property  -dict {PACKAGE_PIN  Y17     IOSTANDARD  LVCMOS25} [get_ports  IO_L19_12_JX2_P]        ; ## U1,Y17,IO_L19_12_JX2_P,JX2,88,IO_L19_12_JX2_P,P13,46
set_property  -dict {PACKAGE_PIN  AB17    IOSTANDARD  LVCMOS25} [get_ports  IO_L20_12_JX2_P]        ; ## U1,AB17,IO_L20_12_JX2_P,JX2,87,IO_L20_12_JX2_P,P13,45
set_property  -dict {PACKAGE_PIN  AA17    IOSTANDARD  LVCMOS25} [get_ports  IO_L19_12_JX2_N]        ; ## U1,AA17,IO_L19_12_JX2_N,JX2,90,IO_L19_12_JX2_N,P13,48
set_property  -dict {PACKAGE_PIN  AB16    IOSTANDARD  LVCMOS25} [get_ports  IO_L20_12_JX2_N]        ; ## U1,AB16,IO_L20_12_JX2_N,JX2,89,IO_L20_12_JX2_N,P13,47
set_property  -dict {PACKAGE_PIN  AC17    IOSTANDARD  LVCMOS25} [get_ports  IO_L21_12_JX2_P]        ; ## U1,AC17,IO_L21_12_JX2_P,JX2,93,IO_L21_12_JX2_P,P13,50
set_property  -dict {PACKAGE_PIN  AA15    IOSTANDARD  LVCMOS25} [get_ports  IO_L22_12_JX2_P]        ; ## U1,AA15,IO_L22_12_JX2_P,JX2,94,IO_L22_12_JX2_P,P13,49
set_property  -dict {PACKAGE_PIN  AC16    IOSTANDARD  LVCMOS25} [get_ports  IO_L21_12_JX2_N]        ; ## U1,AC16,IO_L21_12_JX2_N,JX2,95,IO_L21_12_JX2_N,P13,52
set_property  -dict {PACKAGE_PIN  AA14    IOSTANDARD  LVCMOS25} [get_ports  IO_L22_12_JX2_N]        ; ## U1,AA14,IO_L22_12_JX2_N,JX2,96,IO_L22_12_JX2_N,P13,51

## transceiver loop-backs (on-ccbrk)

set_property  -dict {PACKAGE_PIN  R6}     [get_ports  MGTREFCLK0_112_JX1_P]  ; ## U1,R6,MGTREFCLK0_112_JX1_P (JX1,87)
set_property  -dict {PACKAGE_PIN  R5}     [get_ports  MGTREFCLK0_112_JX1_N]  ; ## U1,R5,MGTREFCLK0_112_JX1_N (JX1,89)
set_property  -dict {PACKAGE_PIN  U6}     [get_ports  MGTREFCLK1_112_JX3_P]  ; ## U1,U6,MGTREFCLK1_112_JX3_P (JX3,2)
set_property  -dict {PACKAGE_PIN  U5}     [get_ports  MGTREFCLK1_112_JX3_N]  ; ## U1,U5,MGTREFCLK1_112_JX3_N (JX3,4)
set_property  -dict {PACKAGE_PIN  AB4}    [get_ports  MGTXRX0_112_JX1_P]     ; ## U1,AB4,MGTXRX0_112_JX1_P   (JX1,88)
set_property  -dict {PACKAGE_PIN  AB3}    [get_ports  MGTXRX0_112_JX1_N]     ; ## U1,AB3,MGTXRX0_112_JX1_N   (JX1,90)
set_property  -dict {PACKAGE_PIN  Y4}     [get_ports  MGTXRX1_112_JX1_P]     ; ## U1,Y4,MGTXRX1_112_JX1_P    (JX1,91)
set_property  -dict {PACKAGE_PIN  Y3}     [get_ports  MGTXRX1_112_JX1_N]     ; ## U1,Y3,MGTXRX1_112_JX1_N    (JX1,93)
set_property  -dict {PACKAGE_PIN  V4}     [get_ports  MGTXRX2_112_JX1_P]     ; ## U1,V4,MGTXRX2_112_JX1_P    (JX1,92)
set_property  -dict {PACKAGE_PIN  V3}     [get_ports  MGTXRX2_112_JX1_N]     ; ## U1,V3,MGTXRX2_112_JX1_N    (JX1,94)
set_property  -dict {PACKAGE_PIN  T4}     [get_ports  MGTXRX3_112_JX1_P]     ; ## U1,T4,MGTXRX3_112_JX1_P    (JX1,97)
set_property  -dict {PACKAGE_PIN  T3}     [get_ports  MGTXRX3_112_JX1_N]     ; ## U1,T3,MGTXRX3_112_JX1_N    (JX1,99)
set_property  -dict {PACKAGE_PIN  AA2}    [get_ports  MGTXTX0_112_JX3_P]     ; ## U1,AA2,MGTXTX0_112_JX3_P   (JX3,8)
set_property  -dict {PACKAGE_PIN  AA1}    [get_ports  MGTXTX0_112_JX3_N]     ; ## U1,AA1,MGTXTX0_112_JX3_N   (JX3,10)
set_property  -dict {PACKAGE_PIN  W2}     [get_ports  MGTXTX1_112_JX3_P]     ; ## U1,W2,MGTXTX1_112_JX3_P    (JX3,13)
set_property  -dict {PACKAGE_PIN  W1}     [get_ports  MGTXTX1_112_JX3_N]     ; ## U1,W1,MGTXTX1_112_JX3_N    (JX3,15)
set_property  -dict {PACKAGE_PIN  U2}     [get_ports  MGTXTX2_112_JX3_P]     ; ## U1,U2,MGTXTX2_112_JX3_P    (JX3,14)
set_property  -dict {PACKAGE_PIN  U1}     [get_ports  MGTXTX2_112_JX3_N]     ; ## U1,U1,MGTXTX2_112_JX3_N    (JX3,16)
set_property  -dict {PACKAGE_PIN  R2}     [get_ports  MGTXTX3_112_JX3_P]     ; ## U1,R2,MGTXTX3_112_JX3_P    (JX3,19)
set_property  -dict {PACKAGE_PIN  R1}     [get_ports  MGTXTX3_112_JX3_N]     ; ## U1,R1,MGTXTX3_112_JX3_N    (JX3,21)

## clocks

#create_clock -name ref_clk      -period  4.00 [get_ports MGTREFCLK0_112_JX1_P]
#create_clock -name xcvr_clk_0   -period  8.00 [get_pins i_system_wrapper/system_i/axi_pz_xcvrlb/inst/g_lanes[0].i_xcvrlb_1/i_xch/i_gtxe2_channel/RXOUTCLK]
#create_clock -name xcvr_clk_1   -period  8.00 [get_pins i_system_wrapper/system_i/axi_pz_xcvrlb/inst/g_lanes[1].i_xcvrlb_1/i_xch/i_gtxe2_channel/RXOUTCLK]
#create_clock -name xcvr_clk_2   -period  8.00 [get_pins i_system_wrapper/system_i/axi_pz_xcvrlb/inst/g_lanes[2].i_xcvrlb_1/i_xch/i_gtxe2_channel/RXOUTCLK]
#create_clock -name xcvr_clk_3   -period  8.00 [get_pins i_system_wrapper/system_i/axi_pz_xcvrlb/inst/g_lanes[3].i_xcvrlb_1/i_xch/i_gtxe2_channel/RXOUTCLK]



#####################################
### from adrv9361z7035_constr.xdc ###
#####################################

# constraints (pzsdr2.e)
# platform_ad9361

set_property  -dict {PACKAGE_PIN  G14  IOSTANDARD LVCMOS18} [get_ports platform_ad9361_config_ENABLE]                ; ## IO_L11P_T1_SRCC_35           U1,G14,IO_L11_35_ENABLE
set_property  -dict {PACKAGE_PIN  F14  IOSTANDARD LVCMOS18} [get_ports platform_ad9361_config_TXNRX]                 ; ## IO_L11N_T1_SRCC_35           U1,F14,IO_L11_35_TXNRX

set_property  -dict {PACKAGE_PIN  D13  IOSTANDARD LVCMOS18} [get_ports CTRL_OUT[0]]                         ; ## IO_L19P_T3_35                U1,D13,IO_L19_35_CTRL_OUT0
set_property  -dict {PACKAGE_PIN  C13  IOSTANDARD LVCMOS18} [get_ports CTRL_OUT[1]]                         ; ## IO_L19N_T3_VREF_35           U1,C13,IO_L19_35_CTRL_OUT1
set_property  -dict {PACKAGE_PIN  C14  IOSTANDARD LVCMOS18} [get_ports CTRL_OUT[2]]                         ; ## IO_L20P_T3_AD6P_35           U1,C14,IO_L20_35_CTRL_OUT2
set_property  -dict {PACKAGE_PIN  B14  IOSTANDARD LVCMOS18} [get_ports CTRL_OUT[3]]                         ; ## IO_L20N_T3_AD6N_35           U1,B14,IO_L20_35_CTRL_OUT3
set_property  -dict {PACKAGE_PIN  A15  IOSTANDARD LVCMOS18} [get_ports CTRL_OUT[4]]                         ; ## IO_L21P_T3_DQS_AD14P_35      U1,A15,IO_L21_35_CTRL_OUT4
set_property  -dict {PACKAGE_PIN  A14  IOSTANDARD LVCMOS18} [get_ports CTRL_OUT[5]]                         ; ## IO_L21N_T3_DQS_AD14N_35      U1,A14,IO_L21_35_CTRL_OUT5
set_property  -dict {PACKAGE_PIN  C12  IOSTANDARD LVCMOS18} [get_ports CTRL_OUT[6]]                         ; ## IO_L22P_T3_AD7P_35           U1,C12,IO_L22_35_CTRL_OUT6
set_property  -dict {PACKAGE_PIN  B12  IOSTANDARD LVCMOS18} [get_ports CTRL_OUT[7]]                         ; ## IO_L22N_T3_AD7N_35           U1,B12,IO_L22_35_CTRL_OUT7
set_property  -dict {PACKAGE_PIN  C2   IOSTANDARD LVCMOS18} [get_ports CTRL_IN[0]]                          ; ## IO_L23P_T3_34                U1,C2,IO_L23_34_CTRL_IN0
set_property  -dict {PACKAGE_PIN  B1   IOSTANDARD LVCMOS18} [get_ports CTRL_IN[1]]                          ; ## IO_L23N_T3_34                U1,B1,IO_L23_34_CTRL_IN1
set_property  -dict {PACKAGE_PIN  B2   IOSTANDARD LVCMOS18} [get_ports CTRL_IN[2]]                          ; ## IO_L24P_T3_34                U1,B2,IO_L24_34_CTRL_IN2
set_property  -dict {PACKAGE_PIN  A2   IOSTANDARD LVCMOS18} [get_ports CTRL_IN[3]]                          ; ## IO_L24N_T3_34                U1,A2,IO_L24_34_CTRL_IN3
set_property  -dict {PACKAGE_PIN  G16  IOSTANDARD LVCMOS18} [get_ports EN_AGC]                              ; ## IO_L10P_T1_AD11P_35          U1,G16,IO_L10_35_EN_AGC
set_property  -dict {PACKAGE_PIN  G15  IOSTANDARD LVCMOS18} [get_ports SYNC_IN]                             ; ## IO_L10N_T1_AD11N_35          U1,G15,IO_L10_35_SYNC_IN
set_property  -dict {PACKAGE_PIN  H16  IOSTANDARD LVCMOS18} [get_ports platform_ad9361_spi_RESETB]                   ; ## IO_0_VRN_35                  U1,H16,IO_00_35_AD9361_RST
set_property  -dict {PACKAGE_PIN  K11  IOSTANDARD LVCMOS18} [get_ports AD9361_CLKSEL]                       ; ## IO_0_VRN_34                  U1,K11,IO_00_34_CLKSEL

set_property  -dict {PACKAGE_PIN  C11  IOSTANDARD LVCMOS18  PULLTYPE PULLUP} [get_ports platform_ad9361_spi_SPI_ENB] ; ## IO_L23P_T3_35                U1,C11,IO_L23_35_SPI_ENB
set_property  -dict {PACKAGE_PIN  B11  IOSTANDARD LVCMOS18} [get_ports platform_ad9361_spi_SPI_CLK]                  ; ## IO_L23N_T3_35                U1,B11,IO_L23_35_SPI_CLK
set_property  -dict {PACKAGE_PIN  A13  IOSTANDARD LVCMOS18} [get_ports platform_ad9361_spi_SPI_DI]                   ; ## IO_L24P_T3_AD15P_35          U1,A13,IO_L24_35_SPI_DI
set_property  -dict {PACKAGE_PIN  A12  IOSTANDARD LVCMOS18} [get_ports platform_ad9361_spi_SPI_DO]                   ; ## IO_L24N_T3_AD15N_35          U1,A12,IO_L24_35_SPI_DO

set_property  -dict {PACKAGE_PIN  K12  IOSTANDARD LVCMOS18} [get_ports AD9361_CLKOUT]                       ; ## IO_25_VRP_35                 U1,K12,IO_25_35_CLKOUT

# iic (ccbrk with loopback drives i2c back to the FPGA)

set_property  -dict {PACKAGE_PIN  AF24 IOSTANDARD LVCMOS25 PULLTYPE PULLUP} [get_ports SCL]                 ; ## IO_L5P_T0_13                 U1,AF24,SCL,JX2,17,I2C_SCL,P2,14,P2,4,U1,Y16,IO_L23_12_JX2_P,JX2,97,LED_GPIO_3,P2,4
set_property  -dict {PACKAGE_PIN  AF25 IOSTANDARD LVCMOS25 PULLTYPE PULLUP} [get_ports SDA]                 ; ## IO_L5N_T0_13                 U1,AF25,SDA,JX2,19,I2C_SDA,P2,16,P2,15,U1,AB24,IO_L06_13_JX2_N,JX2,20,IO_L06_13_JX2_N,P2,15

##    reference-only
##    --------------
##    platform_ad9361 (optional rf-card)
##    --------------------------
##    JX4,1,GPO0
##    JX4,2,GPO1
##    JX4,3,GPO2
##    JX4,4,GPO3
##    JX4,7,AUXADC
##    JX4,8,AUXDAC1
##    JX4,10,AUXDAC2
##    JX4,63,AD9361_CLK

##    fixed-io (ps7) (som only, others are carrier specific)
##    ------------------------------------------------------
##    U1,D26,PS_MIO01_500_QSPI0_SS_B
##    U1,F23,PS_MIO06_500_QSPI0_SCLK
##    U1,E25,PS_MIO02_500_QSPI0_IO0
##    U1,D25,PS_MIO03_500_QSPI0_IO1
##    U1,F24,PS_MIO04_500_QSPI0_IO2
##    U1,C26,PS_MIO05_500_QSPI0_IO3
##    U1,A24,PS_MIO08_500_ETH0_RESETN               (magnetics-RJ45- JX3,47,ETH_PHY_LED0)
##    U1,A19,PS_MIO53_501_ETH0_MDIO                 (magnetics-RJ45- JX3,48,ETH_PHY_LED1)
##    U1,A20,PS_MIO52_501_ETH0_MDC                  (magnetics-RJ45- JX3,51,ETH_MD1_P)
##    U1,G22,PS_MIO22_501_ETH0_RX_CLK               (magnetics-RJ45- JX3,53,ETH_MD1_N)
##    U1,F18,PS_MIO27_501_ETH0_RX_CTL               (magnetics-RJ45- JX3,52,ETH_MD2_P)
##    U1,F20,PS_MIO23_501_ETH0_RX_D0                (magnetics-RJ45- JX3,54,ETH_MD2_N)
##    U1,J19,PS_MIO24_501_ETH0_RX_D1                (magnetics-RJ45- JX3,57,ETH_MD3_P)
##    U1,F19,PS_MIO25_501_ETH0_RX_D2                (magnetics-RJ45- JX3,59,ETH_MD3_N)
##    U1,H17,PS_MIO26_501_ETH0_RX_D3                (magnetics-RJ45- JX3,58,ETH_MD4_P)
##    U1,G21,PS_MIO16_501_ETH0_TX_CLK               (magnetics-RJ45- JX3,60,ETH_MD4_N)
##    U1,F22,PS_MIO21_501_ETH0_TX_CTL
##    U1,G17,PS_MIO17_501_ETH0_TX_D0
##    U1,G20,PS_MIO18_501_ETH0_TX_D1
##    U1,G19,PS_MIO19_501_ETH0_TX_D2
##    U1,H19,PS_MIO20_501_ETH0_TX_D3
##    U1,B21,PS_MIO48_501_JX4,JX4,99,USB_UART_RXD
##    U1,A18,PS_MIO49_501_JX4,JX4,98,USB_UART_TXD
##    U1,C22,PS_MIO40_501_SD0_CLK                   (off-board- JX3,43,SDIO_CLKB1)
##    U1,C19,PS_MIO41_501_SD0_CMD                   (off-board- JX3,34,SDIO_CMDB1)
##    U1,F17,PS_MIO42_501_SD0_DATA0                 (off-board- JX3,37,SDIO_DAT0B1)
##    U1,D18,PS_MIO43_501_SD0_DATA1                 (off-board- JX3,36,SDIO_DAT1B1)
##    U1,E18,PS_MIO44_501_SD0_DATA2                 (off-board- JX3,39,SDIO_DAT2B1)
##    U1,C18,PS_MIO45_501_SD0_DATA3                 (off-board- JX3,38,SDIO_DAT3B1)
##    U1,B22,PS_MIO50_501_SD0_CD                    (off-board- JX3,41,JX3_SD1_CDN)
##    U1,E23,PS_MIO07_500_USB_RESET_B               (usb- JX3,63,USB_ID)
##    U1,D24,PS_MIO09_500_USB_CLK_PD                (usb- JX3,67,USB_OTG_P)
##    U1,E20,PS_MIO29_501_USB0_DIR                  (usb- JX3,69,USB_OTG_N)
##    U1,K19,PS_MIO30_501_USB0_STP                  (usb- JX3,68,USB_VBUS_OTG)
##    U1,E21,PS_MIO31_501_USB0_NXT                  (usb- JX3,70,USB_OTG_CPEN)
##    U1,K16,PS_MIO36_501_USB0_CLK
##    U1,K17,PS_MIO32_501_USB0_D0
##    U1,E22,PS_MIO33_501_USB0_D1
##    U1,J16,PS_MIO34_501_USB0_D2
##    U1,D19,PS_MIO35_501_USB0_D3
##    U1,J18,PS_MIO28_501_USB0_D4
##    U1,D20,PS_MIO37_501_USB0_D5
##    U1,D21,PS_MIO38_501_USB0_D6
##    U1,C21,PS_MIO39_501_USB0_D7
##    U1,A23,UNNAMED_3_ICXC7Z035_I94_PSMIO12        (JX4,86,PS_MIO12_500_JX4)

##    ddr (fixed-io)
##    --------------
##    U1,H24,DDR3_DQS0_P
##    U1,G25,DDR3_DQS0_N
##    U1,L24,DDR3_DQS1_P
##    U1,L25,DDR3_DQS1_N
##    U1,P25,DDR3_DQS2_P
##    U1,R25,DDR3_DQS2_N
##    U1,W24,DDR3_DQS3_P
##    U1,W25,DDR3_DQS3_N
##    U1,J26,DDR3_DQ0
##    U1,F25,DDR3_DQ1
##    U1,J25,DDR3_DQ2
##    U1,G26,DDR3_DQ3
##    U1,H26,DDR3_DQ4
##    U1,H23,DDR3_DQ5
##    U1,J24,DDR3_DQ6
##    U1,J23,DDR3_DQ7
##    U1,K26,DDR3_DQ8
##    U1,L23,DDR3_DQ9
##    U1,M26,DDR3_DQ10
##    U1,K23,DDR3_DQ11
##    U1,M25,DDR3_DQ12
##    U1,N24,DDR3_DQ13
##    U1,M24,DDR3_DQ14
##    U1,N23,DDR3_DQ15
##    U1,R26,DDR3_DQ16
##    U1,P24,DDR3_DQ17
##    U1,N26,DDR3_DQ18
##    U1,P23,DDR3_DQ19
##    U1,T24,DDR3_DQ20
##    U1,T25,DDR3_DQ21
##    U1,T23,DDR3_DQ22
##    U1,R23,DDR3_DQ23
##    U1,V24,DDR3_DQ24
##    U1,U26,DDR3_DQ25
##    U1,U24,DDR3_DQ26
##    U1,U25,DDR3_DQ27
##    U1,W26,DDR3_DQ28
##    U1,Y25,DDR3_DQ29
##    U1,Y26,DDR3_DQ30
##    U1,W23,DDR3_DQ31
##    U1,G24,DDR3_DM0
##    U1,K25,DDR3_DM1
##    U1,P26,DDR3_DM2
##    U1,V26,DDR3_DM3
##    U1,K22,DDR3_A0
##    U1,K20,DDR3_A1
##    U1,N21,DDR3_A2
##    U1,L22,DDR3_A3
##    U1,M20,DDR3_A4
##    U1,N22,DDR3_A5
##    U1,L20,DDR3_A6
##    U1,J21,DDR3_A7
##    U1,T20,DDR3_A8
##    U1,U20,DDR3_A9
##    U1,M22,DDR3_A10
##    U1,H21,DDR3_A11
##    U1,P20,DDR3_A12
##    U1,J20,DDR3_A13
##    U1,R20,DDR3_A14
##    U1,U22,DDR3_BA0
##    U1,T22,DDR3_BA1
##    U1,R22,DDR3_BA2
##    U1,R21,DDR3_CK_P
##    U1,P21,DDR3_CK_N
##    U1,U21,DDR3_CKE
##    U1,H22,DDR3_RST#
##    U1,Y21,DDR3_CS#
##    U1,V22,DDR3_WE#
##    U1,V23,DDR3_RAS#
##    U1,Y23,DDR3_CAS#
##    U1,Y22,DDR3_ODT

##    resets, clock and power controls
##    --------------------------------
##    U1,B24,UNNAMED_3_ICXC7Z035_I94_PSCLK50,33.33MEGHZ
##    U1,A22,PS-SRST#
##    U1,C23,PWR_GD_1.35V
##    JX2,10,PG_1P8V
##    JX2,11,PG_MODULE
##    JX1,5,PWR_ENABLE
##    JX1,6,CARRIER_RESET

##    JTAG
##    ----
##    U1,W11,JTAG_TMS,JX1,2,JTAG_TMS
##    U1,W12,JTAG_TCK,JX1,1,JTAG_TCK
##    U1,V11,JTAG_TDI,JX1,4,FPGA_TDI,JTAG_TDI
##    U1,W9,FPGA_DONE,JX1,8,CFG_DONE

##    GBT I/O (clocks)
##    ----------------
##    U1,R5,UNNAMED_7_CAP_I125_N2 (JX1,87,MGTREFCLK0_112_JX1_P)
##    U1,R6,UNNAMED_7_CAP_I123_N2 (JX1,89,MGTREFCLK0_112_JX1_N)
##    U1,U6,UNNAMED_7_CAP_I126_N2 (JX3,2,MGTREFCLK1_112_JX3_P)
##    U1,U5,UNNAMED_7_CAP_I124_N2 (JX3,4,MGTREFCLK1_112_JX3_N)



##########################################
### from adrv9361z7035_constr_lvds.xdc ###
##########################################

# constraints (pzsdr2.e)
# platform_ad9361

set_property  -dict {PACKAGE_PIN  J14  IOSTANDARD LVDS      DIFF_TERM TRUE} [get_ports platform_ad9361_data_sub_DATA_CLK_P]   ; ## IO_L12P_T1_MRCC_35           U1,J14,IO_L12_MRCC_35_DATA_CLK_P
set_property  -dict {PACKAGE_PIN  H14  IOSTANDARD LVDS      DIFF_TERM TRUE} [get_ports platform_ad9361_data_sub_DATA_CLK_N]   ; ## IO_L12N_T1_MRCC_35           U1,H14,IO_L12_MRCC_35_DATA_CLK_N
set_property  -dict {PACKAGE_PIN  H13  IOSTANDARD LVDS      DIFF_TERM TRUE} [get_ports platform_ad9361_data_sub_RX_FRAME_P]   ; ## IO_L7P_T1_AD2P_35            U1,H13,IO_L07_35_RX_FRAME_P
set_property  -dict {PACKAGE_PIN  H12  IOSTANDARD LVDS      DIFF_TERM TRUE} [get_ports platform_ad9361_data_sub_RX_FRAME_N]   ; ## IO_L7N_T1_AD2N_35            U1,H12,IO_L07_35_RX_FRAME_N
set_property  -dict {PACKAGE_PIN  F12  IOSTANDARD LVDS      DIFF_TERM TRUE} [get_ports platform_ad9361_data_sub_P1_D_5_0[1]]  ; ## IO_L1P_T0_AD0P_35            U1,F12,IO_L01_35_RX_D0_P
set_property  -dict {PACKAGE_PIN  E12  IOSTANDARD LVDS      DIFF_TERM TRUE} [get_ports platform_ad9361_data_sub_P1_D_5_0[0]]  ; ## IO_L1N_T0_AD0N_35            U1,E12,IO_L01_35_RX_D0_N
set_property  -dict {PACKAGE_PIN  E10  IOSTANDARD LVDS      DIFF_TERM TRUE} [get_ports platform_ad9361_data_sub_P1_D_5_0[3]]  ; ## IO_L2P_T0_AD8P_35            U1,E10,IO_L02_35_RX_D1_P
set_property  -dict {PACKAGE_PIN  D10  IOSTANDARD LVDS      DIFF_TERM TRUE} [get_ports platform_ad9361_data_sub_P1_D_5_0[2]]  ; ## IO_L2N_T0_AD8N_35            U1,D10,IO_L02_35_RX_D1_N
set_property  -dict {PACKAGE_PIN  G10  IOSTANDARD LVDS      DIFF_TERM TRUE} [get_ports platform_ad9361_data_sub_P1_D_5_0[5]]  ; ## IO_L3P_T0_DQS_AD1P_35        U1,G10,IO_L03_35_RX_D2_P
set_property  -dict {PACKAGE_PIN  F10  IOSTANDARD LVDS      DIFF_TERM TRUE} [get_ports platform_ad9361_data_sub_P1_D_5_0[4]]  ; ## IO_L3N_T0_DQS_AD1N_35        U1,F10,IO_L03_35_RX_D2_N
set_property  -dict {PACKAGE_PIN  E11  IOSTANDARD LVDS      DIFF_TERM TRUE} [get_ports platform_ad9361_data_sub_P1_D_11_6[1]] ; ## IO_L4P_T0_35                 U1,E11,IO_L04_35_RX_D3_P
set_property  -dict {PACKAGE_PIN  D11  IOSTANDARD LVDS      DIFF_TERM TRUE} [get_ports platform_ad9361_data_sub_P1_D_11_6[0]] ; ## IO_L4N_T0_35                 U1,D11,IO_L04_35_RX_D3_N
set_property  -dict {PACKAGE_PIN  G12  IOSTANDARD LVDS      DIFF_TERM TRUE} [get_ports platform_ad9361_data_sub_P1_D_11_6[3]] ; ## IO_L5P_T0_AD9P_35            U1,G12,IO_L05_35_RX_D4_P
set_property  -dict {PACKAGE_PIN  G11  IOSTANDARD LVDS      DIFF_TERM TRUE} [get_ports platform_ad9361_data_sub_P1_D_11_6[2]] ; ## IO_L5N_T0_AD9N_35            U1,G11,IO_L05_35_RX_D4_N
set_property  -dict {PACKAGE_PIN  F13  IOSTANDARD LVDS      DIFF_TERM TRUE} [get_ports platform_ad9361_data_sub_P1_D_11_6[5]] ; ## IO_L6P_T0_35                 U1,F13,IO_L06_35_RX_D5_P
set_property  -dict {PACKAGE_PIN  E13  IOSTANDARD LVDS      DIFF_TERM TRUE} [get_ports platform_ad9361_data_sub_P1_D_11_6[4]] ; ## IO_L6N_T0_VREF_35            U1,E13,IO_L06_35_RX_D5_N
set_property  -dict {PACKAGE_PIN  K13  IOSTANDARD LVDS}     [get_ports platform_ad9361_data_sub_FB_CLK_P]                     ; ## IO_L8P_T1_AD10P_35           U1,K13,IO_L08_35_FB_CLK_P
set_property  -dict {PACKAGE_PIN  J13  IOSTANDARD LVDS}     [get_ports platform_ad9361_data_sub_FB_CLK_N]                     ; ## IO_L8N_T1_AD10N_35           U1,J13,IO_L08_35_FB_CLK_N
set_property  -dict {PACKAGE_PIN  K15  IOSTANDARD LVDS}     [get_ports platform_ad9361_data_sub_TX_FRAME_P]                   ; ## IO_L9P_T1_DQS_AD3P_35        U1,K15,IO_L09_35_TX_FRAME_P
set_property  -dict {PACKAGE_PIN  J15  IOSTANDARD LVDS}     [get_ports platform_ad9361_data_sub_TX_FRAME_N]                   ; ## IO_L9N_T1_DQS_AD3N_35        U1,J15,IO_L09_35_TX_FRAME_N
set_property  -dict {PACKAGE_PIN  D15  IOSTANDARD LVDS}     [get_ports platform_ad9361_data_sub_P0_D_5_0[1]]                  ; ## IO_L13P_T2_MRCC_35           U1,D15,IO_L13_35_TX_D0_P
set_property  -dict {PACKAGE_PIN  D14  IOSTANDARD LVDS}     [get_ports platform_ad9361_data_sub_P0_D_5_0[0]]                  ; ## IO_L13N_T2_MRCC_35           U1,D14,IO_L13_35_TX_D0_N
set_property  -dict {PACKAGE_PIN  F15  IOSTANDARD LVDS}     [get_ports platform_ad9361_data_sub_P0_D_5_0[3]]                  ; ## IO_L14P_T2_AD4P_SRCC_35      U1,F15,IO_L14_35_TX_D1_P
set_property  -dict {PACKAGE_PIN  E15  IOSTANDARD LVDS}     [get_ports platform_ad9361_data_sub_P0_D_5_0[2]]                  ; ## IO_L14N_T2_AD4N_SRCC_35      U1,E15,IO_L14_35_TX_D1_N
set_property  -dict {PACKAGE_PIN  C17  IOSTANDARD LVDS}     [get_ports platform_ad9361_data_sub_P0_D_5_0[5]]                  ; ## IO_L15P_T2_DQS_AD12P_35      U1,C17,IO_L15_35_TX_D2_P
set_property  -dict {PACKAGE_PIN  C16  IOSTANDARD LVDS}     [get_ports platform_ad9361_data_sub_P0_D_5_0[4]]                  ; ## IO_L15N_T2_DQS_AD12N_35      U1,C16,IO_L15_35_TX_D2_N
set_property  -dict {PACKAGE_PIN  E16  IOSTANDARD LVDS}     [get_ports platform_ad9361_data_sub_P0_D_11_6[1]]                 ; ## IO_L16P_T2_35                U1,E16,IO_L16_35_TX_D3_P
set_property  -dict {PACKAGE_PIN  D16  IOSTANDARD LVDS}     [get_ports platform_ad9361_data_sub_P0_D_11_6[0]]                 ; ## IO_L16N_T2_35                U1,D16,IO_L16_35_TX_D3_N
set_property  -dict {PACKAGE_PIN  B16  IOSTANDARD LVDS}     [get_ports platform_ad9361_data_sub_P0_D_11_6[3]]                 ; ## IO_L17P_T2_AD5P_35           U1,B16,IO_L17_35_TX_D4_P
set_property  -dict {PACKAGE_PIN  B15  IOSTANDARD LVDS}     [get_ports platform_ad9361_data_sub_P0_D_11_6[2]]                 ; ## IO_L17N_T2_AD5N_35           U1,B15,IO_L17_35_TX_D4_N
set_property  -dict {PACKAGE_PIN  B17  IOSTANDARD LVDS}     [get_ports platform_ad9361_data_sub_P0_D_11_6[5]]                 ; ## IO_L18P_T2_AD13P_35          U1,B17,IO_L18_35_TX_D5_P
set_property  -dict {PACKAGE_PIN  A17  IOSTANDARD LVDS}     [get_ports platform_ad9361_data_sub_P0_D_11_6[4]]                 ; ## IO_L18N_T2_AD13N_35          U1,A17,IO_L18_35_TX_D5_N

# clocks
#create_clock -name rx_clk       -period  4 [get_ports rx_clk_in_p]
#create_clock -name platform_ad9361_clk   -period  4 [get_pins i_system_wrapper/system_i/axi_platform_ad9361/clk]

# ----------------------------------------------------------------------------
# INPUT / OUTPUT DELAY constraints - platform_ad9361_dac_sub.hdl
# ----------------------------------------------------------------------------

# ADRV9361 TX_D/TX_FRAME_P
#
# ----- from Vivado GUI:
#                                 _____________
# forwarded clock              __|             |_____________
#                       __    ___:_____     ___:_____    ____
# data at destination   __XXXX___:_____XXXXX___:_____XXXX____
#                            :<--:---->:   :<--:-----:
#                           tsu_r  thd_r  tsu_f  thd_f
#
# tsu_r : Destination device setup time requirement for rising edge
# thd_r : Destination device hold time requirement for rising edge
# tsu_f : Destination device setup time requirement for falling edge
# thd_f : Destination device hold time requirement for falling edge
# trce_dly_max : Maximum board trace delay
# trce_dly_min : Minimum board trace delay
#
# Rise Max = trce_dly_max + tsu_r
# Rise Min = trce_dly_min - thd_r
#
# ----- from AD9361 datasheet:
# t_STx_min = 1
# t_HTx_min = 0
#
# ----- assumed platform_ad9361_data_sub.hdl parameter property/no-OS init_param settings
# ----- (values chosen specifically to meet static timing):
# ------------------------------------------------------------------------------
# | platform_ad9361_data_sub.hdl | no-OS init_param member | value | delay(ns)          |
# | parameter property  |                         |       |                    |
# ------------------------------------------------------------------------------
# | FB_CLK_Delay        | tx_fb_clock_delay       | 1     | 0.3                |
# | TX_Data_Delay       | tx_data_delay           | 0     | 0.0                |
# ------------------------------------------------------------------------------
#
# ----- calculations
# tsu_r = t_STx_min + (FB_CLK_Delay-TX_Data_Delay)*0.3 =  1.3 (AD9361 datasheet only specifies falling edge requirement, but rising is implied since DDR is used)
# thd_r = t_HTx_min - (FB_CLK_Delay-TX_Data_Delay)*0.3 = -0.3 (AD9361 datasheet only specifies falling edge requirement, but rising is implied since DDR is used)
# tsu_f = t_STx_min + (FB_CLK_Delay-TX_Data_Delay)*0.3 =  1.3
# thd_f = t_HTx_min - (FB_CLK_Delay-TX_Data_Delay)*0.3 = -0.3
# trce_dly_max is unknown, so value of 0 is used for calculation
# trce_dly_min is unknown, so value of 0 is used for calculation
# Rise Max = trce_dly_max + tsu_r = 0 + 1.3    = 1.3
# Rise Min = trce_dly_min - thd_r = 0 - (-0.3) = 0.3
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -clock_fall -min -add_delay 0.3 [get_ports {platform_ad9361_data_sub_P0_D_11_6[1]}]
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -clock_fall -max -add_delay 1.3 [get_ports {platform_ad9361_data_sub_P0_D_11_6[1]}]
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -clock_fall -min -add_delay 0.3 [get_ports {platform_ad9361_data_sub_P0_D_11_6[0]}]
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -clock_fall -max -add_delay 1.3 [get_ports {platform_ad9361_data_sub_P0_D_11_6[0]}]
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -min -add_delay 0.3 [get_ports {platform_ad9361_data_sub_P0_D_11_6[1]}]
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -max -add_delay 1.3 [get_ports {platform_ad9361_data_sub_P0_D_11_6[1]}]
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -min -add_delay 0.3 [get_ports {platform_ad9361_data_sub_P0_D_11_6[0]}]
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -max -add_delay 1.3 [get_ports {platform_ad9361_data_sub_P0_D_11_6[0]}]
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -clock_fall -min -add_delay 0.3 [get_ports {platform_ad9361_data_sub_P0_D_11_6[3]}]
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -clock_fall -max -add_delay 1.3 [get_ports {platform_ad9361_data_sub_P0_D_11_6[3]}]
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -clock_fall -min -add_delay 0.3 [get_ports {platform_ad9361_data_sub_P0_D_11_6[2]}]
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -clock_fall -max -add_delay 1.3 [get_ports {platform_ad9361_data_sub_P0_D_11_6[2]}]
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -min -add_delay 0.3 [get_ports {platform_ad9361_data_sub_P0_D_11_6[3]}]
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -max -add_delay 1.3 [get_ports {platform_ad9361_data_sub_P0_D_11_6[3]}]
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -min -add_delay 0.3 [get_ports {platform_ad9361_data_sub_P0_D_11_6[2]}]
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -max -add_delay 1.3 [get_ports {platform_ad9361_data_sub_P0_D_11_6[2]}]
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -clock_fall -min -add_delay 0.3 [get_ports {platform_ad9361_data_sub_P0_D_11_6[5]}]
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -clock_fall -max -add_delay 1.3 [get_ports {platform_ad9361_data_sub_P0_D_11_6[5]}]
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -clock_fall -min -add_delay 0.3 [get_ports {platform_ad9361_data_sub_P0_D_11_6[4]}]
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -clock_fall -max -add_delay 1.3 [get_ports {platform_ad9361_data_sub_P0_D_11_6[4]}]
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -min -add_delay 0.3 [get_ports {platform_ad9361_data_sub_P0_D_11_6[5]}]
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -max -add_delay 1.3 [get_ports {platform_ad9361_data_sub_P0_D_11_6[5]}]
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -min -add_delay 0.3 [get_ports {platform_ad9361_data_sub_P0_D_11_6[4]}]
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -max -add_delay 1.3 [get_ports {platform_ad9361_data_sub_P0_D_11_6[4]}]
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -clock_fall -min -add_delay 0.3 [get_ports {platform_ad9361_data_sub_P0_D_5_0[1]}]
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -clock_fall -max -add_delay 1.3 [get_ports {platform_ad9361_data_sub_P0_D_5_0[1]}]
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -clock_fall -min -add_delay 0.3 [get_ports {platform_ad9361_data_sub_P0_D_5_0[0]}]
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -clock_fall -max -add_delay 1.3 [get_ports {platform_ad9361_data_sub_P0_D_5_0[0]}]
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -min -add_delay 0.3 [get_ports {platform_ad9361_data_sub_P0_D_5_0[1]}]
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -max -add_delay 1.3 [get_ports {platform_ad9361_data_sub_P0_D_5_0[1]}]
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -min -add_delay 0.3 [get_ports {platform_ad9361_data_sub_P0_D_5_0[0]}]
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -max -add_delay 1.3 [get_ports {platform_ad9361_data_sub_P0_D_5_0[0]}]
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -clock_fall -min -add_delay 0.3 [get_ports {platform_ad9361_data_sub_P0_D_5_0[3]}]
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -clock_fall -max -add_delay 1.3 [get_ports {platform_ad9361_data_sub_P0_D_5_0[3]}]
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -clock_fall -min -add_delay 0.3 [get_ports {platform_ad9361_data_sub_P0_D_5_0[2]}]
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -clock_fall -max -add_delay 1.3 [get_ports {platform_ad9361_data_sub_P0_D_5_0[2]}]
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -min -add_delay 0.3 [get_ports {platform_ad9361_data_sub_P0_D_5_0[3]}]
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -max -add_delay 1.3 [get_ports {platform_ad9361_data_sub_P0_D_5_0[3]}]
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -min -add_delay 0.3 [get_ports {platform_ad9361_data_sub_P0_D_5_0[2]}]
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -max -add_delay 1.3 [get_ports {platform_ad9361_data_sub_P0_D_5_0[2]}]
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -clock_fall -min -add_delay 0.3 [get_ports {platform_ad9361_data_sub_P0_D_5_0[5]}]
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -clock_fall -max -add_delay 1.3 [get_ports {platform_ad9361_data_sub_P0_D_5_0[5]}]
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -clock_fall -min -add_delay 0.3 [get_ports {platform_ad9361_data_sub_P0_D_5_0[4]}]
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -clock_fall -max -add_delay 1.3 [get_ports {platform_ad9361_data_sub_P0_D_5_0[4]}]
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -min -add_delay 0.3 [get_ports {platform_ad9361_data_sub_P0_D_5_0[5]}]
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -max -add_delay 1.3 [get_ports {platform_ad9361_data_sub_P0_D_5_0[5]}]
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -min -add_delay 0.3 [get_ports {platform_ad9361_data_sub_P0_D_5_0[4]}]
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -max -add_delay 1.3 [get_ports {platform_ad9361_data_sub_P0_D_5_0[4]}]
# TX_FRAME_P
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -clock_fall -min -add_delay 0.3 [get_ports {platform_ad9361_data_sub_TX_FRAME_P}]
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -clock_fall -max -add_delay 1.3 [get_ports {platform_ad9361_data_sub_TX_FRAME_P}]
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -clock_fall -min -add_delay 0.3 [get_ports {platform_ad9361_data_sub_TX_FRAME_N}]
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -clock_fall -max -add_delay 1.3 [get_ports {platform_ad9361_data_sub_TX_FRAME_N}]

# ----------------------------------------------------------------------------
# INPUT / OUTPUT DELAY constraints - platform_ad9361_data_sub.hdl
# ----------------------------------------------------------------------------

# from AD9361 datasheet
set AD9361_ENABLE_t_SC_min 1.0;
set AD9361_ENABLE_t_HC_min 0.0;

set AD9361_ENABLE_tsu_r $AD9361_ENABLE_t_SC_min;
set AD9361_ENABLE_thd_r $AD9361_ENABLE_t_HC_min;

# assume 0 for now, can measure later if necessary
set AD9361_ENABLE_trce_dly_max 0;
set AD9361_ENABLE_trce_dly_min 0;

# see Vivado constraints wizard for these formulas
set AD9361_ENABLE_Rise_Max [expr $AD9361_ENABLE_trce_dly_max + $AD9361_ENABLE_tsu_r];
set AD9361_ENABLE_Rise_Min [expr $AD9361_ENABLE_trce_dly_min - $AD9361_ENABLE_thd_r];

set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -min -add_delay $AD9361_ENABLE_Rise_Min [get_ports {platform_ad9361_config_TXNRX}]
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -max -add_delay $AD9361_ENABLE_Rise_Max [get_ports {platform_ad9361_config_TXNRX}]

# from AD9361 datasheet
set AD9361_TXNRX_t_SC_min 1.0;
set AD9361_TXNRX_t_HC_min 0.0;

set AD9361_TXNRX_tsu_r $AD9361_TXNRX_t_SC_min;
set AD9361_TXNRX_thd_r $AD9361_TXNRX_t_HC_min;

# assume 0 for now, can measure later if necessary
set AD9361_TXNRX_trce_dly_max 0;
set AD9361_TXNRX_trce_dly_min 0;

# see Vivado constraints wizard for these formulas
set AD9361_TXNRX_Rise_Max [expr $AD9361_TXNRX_trce_dly_max + $AD9361_TXNRX_tsu_r];
set AD9361_TXNRX_Rise_Min [expr $AD9361_TXNRX_trce_dly_min - $AD9361_TXNRX_thd_r];

set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -min -add_delay $AD9361_TXNRX_Rise_Min [get_ports {platform_ad9361_config_ENABLE}]
set_output_delay -clock [get_clocks {platform_ad9361_data_sub_FB_CLK_P}] -max -add_delay $AD9361_TXNRX_Rise_Max [get_ports {platform_ad9361_config_ENABLE}]

# ----------------------------------------------------------------------------
# CLOCK DOMAIN CROSSING / FALSE PATH constraints - platform_ad9361_data_sub.hdl
# ----------------------------------------------------------------------------

# disable timing check among paths between AD9361 DATA_CLK_P and control plane clock domains (which are asynchronous)
set_clock_groups -asynchronous -group [get_clocks {platform_ad9361_data_sub_DATA_CLK_P}] -group [get_clocks {clk_fpga_0}]

# ----------------------------------------------------------------------------
# CLOCK DOMAIN CROSSING / FALSE PATH constraints - platform_ad9361_dac_sub.hdl
# ----------------------------------------------------------------------------
# disable timing check among paths between AD9361 DATA_CLK_P/2 clock domain and control plane clock domains (which are asynchronous)
set_clock_groups -asynchronous -group [get_clocks clk_fpga_0] -group [get_clocks -of_objects [get_pins ftop/pfconfig_i/platform_ad9361_dac_sub_i/worker/data_mode_lvds.BUFR_inst/O]]
